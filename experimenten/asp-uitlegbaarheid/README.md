# Answer Set Programming experiment - PRA Article 7 AOW

## Description

This folder contains a small experiment on how to use Answer Set Programming (ASP) for writing rules as code with the old age pension eligible age (pensioengerechtigde leeftijd) as defined in law (Article 7 , 7a AOW) based on the flint-visualisation experiment

Clingo was used for this which can be found at : https://potassco.org/clingo/

The .html has an older version of the ASP script which only calculates possible solutions
The .lp uses embedded python (requires a specific version of clingo) to generate a graph (like can be found in pgl-test2.png) and explanatory text using matpltlib and numpy 

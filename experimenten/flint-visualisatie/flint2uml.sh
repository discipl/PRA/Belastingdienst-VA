#!/bin/sh
if [ -z "$1" ]
  then
    echo "Usage : ./flint2uml.sh saved-from-flint-editor.json"
fi
if command -v jq >/dev/null 2>&1 ; then
    echo "transforming flint model in $1 to diagram in plantuml.png..."
else
    echo "jq not found.. please install (Ubuntu: sudo apt-get install jq)"
fi
echo "@startuml\nskinparam {\n\twrapWidth 400\n}\n" > plantuml.txt
cat $1 | jq -r '.frames[] | (.id) |= (split("-") | join("")) | (.comments += ["-"]) | "class "+.id+" {\n\t="+.label+"\ncomment: "+.comments[0]?+"\n\ncitaat : \""+.fact+"\"\n}\n"' >> plantuml.txt
cat $1 | jq -r '.frames[] | (.id) |= (split("-") | join("")) | (.subdivision.children[]?.frame) |= (split("-") | join("")) | .id+" *-- "+.subdivision.children[]?.frame+"\n"' >> plantuml.txt
cat $1 | jq -r '.frames[] | (.id) |= (split("-") | join("")) | select(.subdivision.frame) | (.subdivision.frame) |= (split("-") | join("")) | .id+" *-- "+.subdivision.frame?+"\n"' >> plantuml.txt
echo "@enduml" >> plantuml.txt
echo "saved plantUML source in plantuml.txt..."
echo "rendering diagram to plantuml.png..."
java -jar plantuml-1.2024.3.jar plantuml.txt

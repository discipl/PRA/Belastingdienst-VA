# Flint model voor artikel 7 AOW - BD pensioen use case - met plantUML visualisatie

### Installatie

 - clone de flint interpretation editor lokaal in ./interpretation-editor : git clone https://gitlab.com/normativesystems/ui/interpretation-editor.git
 - kopieer de AOW wet (Algemene-Ouderdoms-Wet.json) naar ./interpretation-editor/gui/public/sources/
 - kopieer sources.json naar ./interpretation-editor/gui/public/
 - download plantuml jarfile : wget https://github.com/plantuml/plantuml/releases/download/v1.2024.3/plantuml-1.2024.3.jar
 
### Gebruik

- Run de interpretation editor, zie de README : https://gitlab.com/normativesystems/ui/interpretation-editor
- De flint-aow-artikel7.json is in te laden in de interpretation editor na starten willekeurige taak en selecteren van de Algemene Ouderdomswet die in het lijstje sources zou moeten staan.
- Genereer (opnieuw) plantuml.txt en plantuml.png vanuit de in flint-aow-artikel7.json opgeslagen interpretatie: ./flint2uml.sh flint-aow-artikel7.json
- Je kunt vanuit de editor een aangepaste versie opslaan en plaatje van genereren. De plantuml.txt kan opgenomen worden in Markdown in gitlab

### interpretatie AOW artikel 7 (enkel op het niveau van Flint)

```plantuml
@startuml
skinparam {
	wrapWidth 400
}

class 3d22decb2daa4d3f96914a316f29d84c {
	=heeft pensioengerechtigde leeftijd bereikt
comment: -

citaat : "de pensioengerechtigde leeftijd heeft bereikt,"
}

class ddeb36c43297445e909f7b778046e4fb {
	=minimaal 1 kalenderjaar verzekerd
comment: -

citaat : "b. ingevolge deze wet minimaal één kalenderjaar verzekerd is geweest in het tijdvak, aanvangende met de dag waarop de aanvangsleeftijd is bereikt en eindigende met de dag voorafgaande aan de dag waarop de pensioengerechtigde leeftijd is bereikt."
}

class 27c84c63e5f44bd4b67a09d2f6fc767f {
	=Recht op Ouderdomspensioen
comment: Gegeven de persoon in kwestie, en in het algemeen heeft een persoon altijd een leeftijd, en gegeven de regels waarmee de pensioengerechtigde leeftijd en aanvangsleeftijd worden vastgesteld, heeft iemand het recht op ouderdomspensioen wanneer die de pensioengerechtigde leeftijd heeft bereikt en minimaal 1 kalenderjaar is verzekerd geweest sinds de aanvangsleeftijd die voor de persoon in kwestie geldt.

citaat : "1. Recht op ouderdomspensioen overeenkomstig de bepalingen van deze wet heeft"
}

class 28b735f5d4ff402bb2bf4d023ba6069b {
	=persoon in kwestie
comment: In het algemeen heeft een persoon een leeftijd hetgeen bepaald kan worden aan de hand van zijn of haar geboortedatum. Deze moet bekend zijn. Link met BasisRegister Personen

citaat : "degene"
}

class 42cede4c6dc647eeb36a06b8fbc5bad3 {
	=Geldende pensioengerechtige leeftijd en aanvangsleeftijd
comment: -

citaat : "Artikel 7a
1. De pensioengerechtigde leeftijd en de aanvangsleeftijd zijn:"
}

class bfe2ce88043b4d43b5bb3b297b26c442 {
	=Geldende pensioengerechtige leeftijd en aanvangsleeftijd voor 2026
comment: -

citaat : "a. vóór 1 januari 2013: 65, respectievelijk 15 jaar;
b. in 2013: 65 jaar en één maand, respectievelijk 15 jaar en één maand;
c. in 2014: 65 jaar en twee maanden, respectievelijk 15 jaar en twee maanden;
d. in 2015: 65 jaar en drie maanden, respectievelijk 15 jaar en drie maanden;
e. in 2016: 65 jaar en zes maanden, respectievelijk 15 jaar en zes maanden;
f. in 2017: 65 jaar en negen maanden, respectievelijk 15 jaar en negen maanden;
g. in 2018: 66 jaar, respectievelijk 16 jaar;
h. in 2019: 66 jaar en vier maanden, respectievelijk 16 jaar en vier maanden;
i. in 2020: 66 jaar en vier maanden, respectievelijk 16 jaar en vier maanden;
j. in 2021: 66 jaar en vier maanden, respectievelijk 16 jaar en vier maanden;
k. in 2022: 66 jaar en zeven maanden, respectievelijk 16 jaar en zeven maanden;
l. in 2023: 66 jaar en tien maanden, respectievelijk 16 jaar en tien maanden;
m. in 2024: 67 jaar, respectievelijk 17 jaar;
n. in 2025: 67 jaar, respectievelijk 17 jaar;"
}

class f5cb5eaa3c7f425ca24e3fe7abe5f133 {
	=Geldende pensioengerechtige leeftijd en aanvangsleeftijd vanaf 2026
comment: -

citaat : "o. in 2026 en de kalenderjaren daarna: de pensioengerechtigde leeftijd en de aanvangsleeftijd, die jaarlijks op basis van de verhoging van de pensioengerechtigde leeftijd en de aanvangsleeftijd op grond van het tweede lid worden vastgesteld."
}

class 6d64bb0431214dfe859c9baea5f4bbbb {
	=Verhoging pensioengerechtigde leeftijd en aanvangsleeftijd vanaf 2026
comment: -

citaat : "2. De verhoging van de pensioengerechtigde leeftijd en de aanvangsleeftijd in 2026 en de kalenderjaren daarna wordt jaarlijks, voor de eerste maal uiterlijk op 1 januari 2021 voor het jaar 2026, vastgesteld volgens de formule:"
}

class 30a464f9c9724b4690990266b4ead5aa {
	=Verhogingsformule vanaf 2026
comment: -

citaat : "V = 2/3 * (L – 20,64) – (P – 67)"
}

class 2fafa81ceb6a4ec394d7725327cfa948 {
	=V uit verhogingsformule
comment: -

citaat : "V staat voor de periode waarmee de pensioengerechtigde leeftijd respectievelijk aanvangsleeftijd wordt verhoogd, uitgedrukt in perioden van een jaar;"
}

class 025a2eb3af884d1fbd059501f0d6f14b {
	=L uit verhogingsformule
comment: -

citaat : "L staat voor de geraamde macro gemiddelde resterende levensverwachting op 65-jarige leeftijd in het kalenderjaar van verhoging;"
}

class ea030dd2d02d4642969aaf5bcb31737c {
	=P uit verhogingsformule
comment: -

citaat : "P staat voor de pensioengerechtigde leeftijd in het kalenderjaar voorafgaande aan het kalenderjaar van verhoging."
}

class 8065a52ca1904c33951036fa23596f8d {
	=eenmaal pensioengerechtigd dan doen pensioengerechtigde leeftijd en aanvangsleeftijd niet ter zake meer
comment: -

citaat : "Op pensioengerechtigden die in een bepaald kalenderjaar de pensioengerechtigde leeftijd hebben bereikt zijn de pensioengerechtigde leeftijd en de aanvangsleeftijd in de kalenderjaren daarna niet van toepassing."
}

class 4332b1a891764436aca9334e346f06bf {
	=vaststelling regels herleiding gedeelte tot hele kalenderjaren
comment: -

citaat : "2. Bij ministeriële regeling worden regels gesteld omtrent de herleiding van gedeelten van kalenderjaren tot gehele kalenderjaren."
}

class 77a6cc39a9974099b59aad3ce720bae5 {
	=Limitering van verhoging
comment: -

citaat : "ndien V negatief is of minder dan 0,25 bedraagt, wordt deze gesteld op 0. Indien V 0,25 of meer bedraagt, wordt deze gesteld op drie maanden."
}

class 42c712f696fd4f44a60ec6032a9392df {
	=Inwerkingtreding verhoging vanaf 2026
comment: -

citaat : "3. De verhoging, bedoeld in het tweede lid, treedt telkens in werking vijf jaar na de uiterste datum van vaststelling, bedoeld in het tweede lid, voor de eerste maal met ingang van 1 januari 2026."
}

class b369149554b74982bfcc6dfa7dce5681 {
	=leeftijd
comment: -

citaat : "leeftijd"
}

27c84c63e5f44bd4b67a09d2f6fc767f *-- 28b735f5d4ff402bb2bf4d023ba6069b

27c84c63e5f44bd4b67a09d2f6fc767f *-- 4332b1a891764436aca9334e346f06bf

27c84c63e5f44bd4b67a09d2f6fc767f *-- 3d22decb2daa4d3f96914a316f29d84c

27c84c63e5f44bd4b67a09d2f6fc767f *-- ddeb36c43297445e909f7b778046e4fb

42cede4c6dc647eeb36a06b8fbc5bad3 *-- bfe2ce88043b4d43b5bb3b297b26c442

42cede4c6dc647eeb36a06b8fbc5bad3 *-- f5cb5eaa3c7f425ca24e3fe7abe5f133

42cede4c6dc647eeb36a06b8fbc5bad3 *-- 8065a52ca1904c33951036fa23596f8d

f5cb5eaa3c7f425ca24e3fe7abe5f133 *-- 6d64bb0431214dfe859c9baea5f4bbbb

6d64bb0431214dfe859c9baea5f4bbbb *-- 30a464f9c9724b4690990266b4ead5aa

6d64bb0431214dfe859c9baea5f4bbbb *-- 2fafa81ceb6a4ec394d7725327cfa948

6d64bb0431214dfe859c9baea5f4bbbb *-- 77a6cc39a9974099b59aad3ce720bae5

6d64bb0431214dfe859c9baea5f4bbbb *-- 025a2eb3af884d1fbd059501f0d6f14b

6d64bb0431214dfe859c9baea5f4bbbb *-- ea030dd2d02d4642969aaf5bcb31737c

6d64bb0431214dfe859c9baea5f4bbbb *-- 42c712f696fd4f44a60ec6032a9392df

28b735f5d4ff402bb2bf4d023ba6069b *-- b369149554b74982bfcc6dfa7dce5681

4332b1a891764436aca9334e346f06bf *-- 42cede4c6dc647eeb36a06b8fbc5bad3

@enduml
```

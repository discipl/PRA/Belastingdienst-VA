% operators
:- use_module(library(clpfd)).

aantal_dagen_in_maand_in_jaar(31, 1, _).
aantal_dagen_in_maand_in_jaar(28, 2, Y) :-
    #\ (Y mod 400 #= 0 #\/ (Y mod 4 #= 0 #/\ Y mod 100 #\= 0)).
aantal_dagen_in_maand_in_jaar(29, 2, Y) :-
    Y mod 400 #= 0 #\/ (Y mod 4 #= 0 #/\ Y mod 100 #\= 0).
aantal_dagen_in_maand_in_jaar(31, 3, _).
aantal_dagen_in_maand_in_jaar(30, 4, _).
aantal_dagen_in_maand_in_jaar(31, 5, _).
aantal_dagen_in_maand_in_jaar(30, 6, _).
aantal_dagen_in_maand_in_jaar(31, 7, _).
aantal_dagen_in_maand_in_jaar(31, 8, _).
aantal_dagen_in_maand_in_jaar(30, 9, _).
aantal_dagen_in_maand_in_jaar(31, 10, _).
aantal_dagen_in_maand_in_jaar(30, 11, _).
aantal_dagen_in_maand_in_jaar(31, 12, _).

gregorian(Y, M, D) :-
    Y in -4713..3267,
    M in 1..12,
    (   (D in 1..28)
    #\/ (M #\= 2 #/\ D in 29..30)
    #\/ (M in 1 \/ 3 \/ 5 \/ 7 \/ 8 \/ 10 \/ 12 #/\ D #= 31)
    #\/ (M #= 2 #/\ D #= 29 #/\ Y mod 400 #= 0)
    #\/ (M #= 2 #/\ D #= 29 #/\ Y mod 4 #= 0 #/\ Y mod 100 #\= 0)
    ).

valide_datum(J,M,D) :- gregorian(J,M,D).
valide_datum(datum(J,M,D)) :- valide_datum(J,M,D).

datum_in_jaar(datum(Jaar, _, _), Jaar).

datum_voor_datum(datum(J1, M1, D1), datum(J2, M2, D2)) :-
    valide_datum(J1, M1, D1),
    valide_datum(J2, M2, D2),
    J1 #< J2.
datum_voor_datum(datum(J1, M1, D1), datum(J2, M2, D2)) :-
    valide_datum(J1, M1, D1),
    valide_datum(J2, M2, D2),
    J1 #= J2,
    M1 #< M2.
datum_voor_datum(datum(J1, M1, D1), datum(J2, M2, D2)) :-
    valide_datum(J1, M1, D1),
    valide_datum(J2, M2, D2),
    J1 #= J2,
    M1 #= M2,
    D1 #< D2.

datum_na_datum(Datum1, Datum2) :-
    datum_voor_datum(Datum2, Datum1).

laatste_maand_is_nog_niet_verstreken_op_datum_vanaf_datum_met_restdagen(1, datum(J2, M2, D2), datum(J1, M1, D1), Restdagen) :-
    valide_datum(J1, M1, D1),
    valide_datum(J2, M2, D2),
    aantal_dagen_in_maand_in_jaar(DagenM2, M2, J2),
    min(D1, DagenM2) #> D2,
    aantal_dagen_in_maand_in_jaar(DagenM1, M1, J1),
    Restdagen in 1..30 #/\
    Restdagen #= DagenM1-(D1-D2).

laatste_maand_is_nog_niet_verstreken_op_datum_vanaf_datum_met_restdagen(0, datum(J2, M2, D2), datum(J1, M1, D1), Restdagen) :-
    valide_datum(J1, M1, D1),
    valide_datum(J2, M2, D2),
    aantal_dagen_in_maand_in_jaar(DagenM2, M2, J2),
    min(D1, DagenM2) #=< D2,
    Restdagen in -3..30 #/\
    Restdagen #= D2-D1.

datum_datum_maandenverschil_restdagen(datum(J1, M1, D1), datum(J2, M2, D2), Maandenverschil, Restdagen) :-
    datum_voor_datum(datum(J1, M1, D1), datum(J2, M2, D2)),
    laatste_maand_is_nog_niet_verstreken_op_datum_vanaf_datum_met_restdagen(LaatsteMaandNogNietVerstreken, datum(J2, M2, D2), datum(J1, M1, D1), Restdagen),
    Maandenverschil #= (J2-J1) * 12 + (M2-M1) - LaatsteMaandNogNietVerstreken.

datum_datum_maandenverschil_restdagen(Datum1, Datum2, NegatiefMaandenverschil, NegatieveRestdagen) :-
    datum_na_datum(Datum1, Datum2),
    datum_datum_maandenverschil_restdagen(Datum2, Datum1, Maandenverschil, Restdagen),
    NegatiefMaandenverschil #= -Maandenverschil,
    NegatieveRestdagen #= -Restdagen.

leeftijd_in_maanden(leeftijd(Jaren, Maanden), Maandentotaal) :-
    Maanden #= Maandentotaal rem 12,
    Jaren #= Maandentotaal // 12.

heeft_leeftijd_op_datum(Geboortedatum, Leeftijd, Datum) :-
    datum_datum_maandenverschil_restdagen(Geboortedatum, Datum, Maandentotaal, _),
    leeftijd_in_maanden(Leeftijd, Maandentotaal).

bereikt_leeftijd_op_datum(Geboortedatum, Leeftijd, Datum) :-
    Restdagen #=< 0, % kan negatief zijn: wanneer de dag van Geboortedatum niet in de maand van Datum zit
    datum_datum_maandenverschil_restdagen(Geboortedatum, Datum, Maandentotaal, Restdagen),
    leeftijd_in_maanden(Leeftijd, Maandentotaal).

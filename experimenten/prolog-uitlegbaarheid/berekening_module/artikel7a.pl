% Artikel 7a
% ``De pensioengerechtigde leeftijd is:''

    % Artikel 7a 1 a.
    % ``vóór 1 januari 2013: 65 jaar;''
    de_pensioengerechtigde_leeftijd_op_datum(leeftijd(65, 0), Datum)       %  ``65 jaar''
        :-                                                                 %  ``: ''
            datum_voor_datum(Datum, datum(2013, 1, 1))                     %  ``vóór 1 januari 2013''
        . % er volgt nog een extra alternatieve pensioengerechtigde leeftijd  ``;''

    % Artikel 7a 1 b.
    % ``in 2013: 65 jaar en één maand;''
    de_pensioengerechtigde_leeftijd_op_datum(leeftijd(65, 1), Datum)       %  ``65 jaar en één maand''
        :-                                                                 %  ``: ''
            datum_in_jaar(Datum, 2013)                                     %  ``in 2013''
        . % er volgt nog een extra alternatieve pensioengerechtigde leeftijd  ``;''

    % Artikel 7a 1 c.
    % ``in 2014: 65 jaar en twee maanden;''
    de_pensioengerechtigde_leeftijd_op_datum(leeftijd(65, 2), Datum)       %  ``65 jaar en twee maanden''
        :-                                                                 %  ``: ''
            datum_in_jaar(Datum, 2014)                                     %  ``in 2014''
        . % er volgt nog een extra alternatieve pensioengerechtigde leeftijd  ``;''

    % Artikel 7a 1 d.
    % ``in 2015: 65 jaar en drie maanden;''
    de_pensioengerechtigde_leeftijd_op_datum(leeftijd(65, 3), Datum)       %  ``65 jaar en drie maanden''
        :-                                                                 %  ``: ''
            datum_in_jaar(Datum, 2015)                                     %  ``in 2015''
        . % er volgt nog een extra alternatieve pensioengerechtigde leeftijd  ``;''

    % Artikel 7a 1 e.
    % ``in 2016: 65 jaar en zes maanden;''
    de_pensioengerechtigde_leeftijd_op_datum(leeftijd(65, 6), Datum)       %  ``65 jaar en zes maanden''
        :-                                                                 %  ``: ''
            datum_in_jaar(Datum, 2016)                                     %  ``in 2016''
        . % er volgt nog een extra alternatieve pensioengerechtigde leeftijd  ``;''

    % Artikel 7a 1 f.
    % ``in 2017: 65 jaar en negen maanden;''
    de_pensioengerechtigde_leeftijd_op_datum(leeftijd(65, 9), Datum)       %  ``65 jaar en negen maanden''
        :-                                                                 %  ``: ''
            datum_in_jaar(Datum, 2017)                                     %  ``in 2017''
        . % er volgt nog een extra alternatieve pensioengerechtigde leeftijd  ``;''

    % Artikel 7a 1 g.
    % ``in 2018: 66 jaar;''
    de_pensioengerechtigde_leeftijd_op_datum(leeftijd(66, 0), Datum     )  %  ``66 jaar''
        :-                                                                 %  ``: ''
            datum_in_jaar(Datum, 2018)                                     %  ``in 2018''
        . % er volgt nog een extra alternatieve pensioengerechtigde leeftijd  ``;''

    % Artikel 7a 1 h.
    % ``in 2019: 66 jaar en vier maanden;''
    de_pensioengerechtigde_leeftijd_op_datum(leeftijd(66, 4), Datum)       %  ``66 jaar en vier maanden''
        :-                                                                 %  ``: ''
            datum_in_jaar(Datum, 2019)                                     %  ``in 2019''
        . % er volgt nog een extra alternatieve pensioengerechtigde leeftijd  ``;''

    % Artikel 7a 1 i.
    % ``in 2020: 66 jaar en vier maanden;''
    de_pensioengerechtigde_leeftijd_op_datum(leeftijd(66, 4), Datum)       %  ``66 jaar en vier maanden''
        :-                                                                 %  ``: ''
            datum_in_jaar(Datum, 2020)                                     %  ``in 2020''
        . % er volgt nog een extra alternatieve pensioengerechtigde leeftijd  ``;''

    % Artikel 7a 1 j.
    % ``in 2021: 66 jaar en vier maanden;''
    de_pensioengerechtigde_leeftijd_op_datum(leeftijd(66, 4), Datum)       %  ``66 jaar en vier maanden''
        :-                                                                 %  ``: ''
            datum_in_jaar(Datum, 2021)                                     %  ``in 2021''
        . % er volgt nog een extra alternatieve pensioengerechtigde leeftijd  ``;''

    % Artikel 7a 1 k.
    % ``in 2022: 66 jaar en zeven maanden;''
    de_pensioengerechtigde_leeftijd_op_datum(leeftijd(66, 7), Datum)       %  ``66 jaar en zeven maanden''
        :-                                                                 %  ``: ''
            datum_in_jaar(Datum, 2022)                                     %  ``in 2022''
        . % er volgt nog een extra alternatieve pensioengerechtigde leeftijd  ``;''

    % Artikel 7a 1 l.
    % ``in 2023: 66 jaar en tien maanden;''
    de_pensioengerechtigde_leeftijd_op_datum(leeftijd(66, 10), Datum)      %  ``66 jaar en tien maanden''
        :-                                                                 %  ``: ''
            datum_in_jaar(Datum, 2023)                                     %  ``in 2023''
        . % er volgt nog een extra alternatieve pensioengerechtigde leeftijd  ``;''

    % Artikel 7a 1 m.
    % ``in 2024: 67 jaar;''
    de_pensioengerechtigde_leeftijd_op_datum(leeftijd(67, 0), Datum)       %  ``67 jaar''
        :-                                                                 %  ``: ''
            datum_in_jaar(Datum, 2024)                                     %  ``in 2024''
        . % er volgt nog een extra alternatieve pensioengerechtigde leeftijd  ``;''

    % Artikel 7a 1 n.
    % ``in 2025: 67 jaar;''
    de_pensioengerechtigde_leeftijd_op_datum(leeftijd(67, 0), Datum)       %  ``67 jaar''
        :-                                                                 %  ``: ''
            datum_in_jaar(Datum, 2025)                                     %  ``in 2025''
        . % er volgt nog een extra alternatieve pensioengerechtigde leeftijd  ``;''

    % Artikel 7a 1 o.
    % ``in 2026 en de kalenderjaren daarna: de pensioengerechtigde leeftijd, die jaarlijks op basis
    % van de verhoging van de pensioengerechtigde leeftijd op grond van het tweede lid wordt
    % vastgesteld.''
    de_pensioengerechtigde_leeftijd_op_datum(nog_niet_vastgesteld, Datum)         %  ``wordt vastgesteld''
        :-                                                                        %  ``: ''
            datum_in_jaar(Datum, Kalenderjaar),
            (   Kalenderjaar #= 2026                                              %  ``in 2026''
            #\/ Kalenderjaar #> 2026                                              %  ``en de kalenderjaren daarna''
            ),
        !. % er volgen geen andere alternatieve pensioengerechtigde leeftijden meer  ``.''

% de aanwezigheid van de tabel in Artikel 7a 1 geeft aan dat je een pensioengerechtigde leeftijd
% bereikt wanneer deze gelijk is aan je eigen leeftijd op een moment dat de pensioengerechtigde
% leeftijd van kracht is
bereikt_de_pensioengerechtigde_leeftijd_op_datum(Geboortedatum, Leeftijd, Datum)
    :-
        bereikt_leeftijd_op_datum(Geboortedatum, Leeftijd, Datum),
        de_pensioengerechtigde_leeftijd_op_datum(Leeftijd, Datum)
    .


% Artikel 7a 1
% ``Op pensioengerechtigden die ...''
    % ... in een bepaald kalenderjaar de pensioengerechtigde leeftijd hebben bereikt ...''
    bereikt_pensioengerechtigde_leeftijd_in_kalenderjaar(Geboortedatum, Kalenderjaar)
        :-
            datum_in_jaar(Datum, Kalenderjaar),
            bereikt_de_pensioengerechtigde_leeftijd_op_datum(Geboortedatum, _, Datum)
        .

    % ``... is de pensioengerechtigde leeftijd in de kalenderjaren daarna ...''
    bereikt_pensioengerechtigde_leeftijd_in_een_eerder_kalenderjaar(Geboortedatum, Kalenderjaar)
        :-
            % ``bepaald kalenderjaar'' is eerder dan Kalenderjaar want er wordt gesproken over ``kalenderjaren daarna''
            BepaaldKalenderjaar #< Kalenderjaar,
            bereikt_pensioengerechtigde_leeftijd_in_kalenderjaar(Geboortedatum, BepaaldKalenderjaar)
        .
    % ``... niet van toepassing.''
    geboortedatum_waarbij_de_pensioengerechtigde_leeftijd_in_kalenderjaar_NIET_van_toepassing_is(Geboortedatum, Kalenderjaar)
        :-
            bereikt_pensioengerechtigde_leeftijd_in_een_eerder_kalenderjaar(Geboortedatum, Kalenderjaar)
        .

    % de pensioengerechtigde leeftijd moet logischerwijs WEL van toepassing zijn wanneer niet kan
    % worden vastgesteld (de \+ operator) dat het NIET van toepassing is
    geboortedatum_waarbij_de_pensioengerechtigde_leeftijd_in_kalenderjaar_WEL_van_toepassing_is(Geboortedatum, Kalenderjaar)
        :-
            \+ geboortedatum_waarbij_de_pensioengerechtigde_leeftijd_in_kalenderjaar_NIET_van_toepassing_is(Geboortedatum, Kalenderjaar)
        .

% doordat Artikel 7a 1 alleen expliciet maakt wat er NIET van toepassing is wordt er geimpliceerd
% dat het tegenovergestelde WEL van toepassing is
wordt_pensioengerechtigd_bij_leeftijd_op_datum(Geboortedatum, Pensioenleeftijd, PensioenDatum)
    :-
        bereikt_de_pensioengerechtigde_leeftijd_op_datum(Geboortedatum, Pensioenleeftijd, PensioenDatum),
        datum_in_jaar(PensioenDatum, PensioenKalenderjaar),
        geboortedatum_waarbij_de_pensioengerechtigde_leeftijd_in_kalenderjaar_WEL_van_toepassing_is(Geboortedatum, PensioenKalenderjaar)
    .

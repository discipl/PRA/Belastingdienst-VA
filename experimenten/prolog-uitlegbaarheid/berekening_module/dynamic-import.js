import { SWIPL } from "https://SWI-Prolog.github.io/npm-swipl-wasm/3/7/8/dynamic-import.js";

const notificatiePL = `notificatie(Vandaag, Geboortedatum, Restdagen, "U gaat over minder dan een maand met pensioen! Bekijk nu in de PRA de dingen om op te letten.") :-
    wordt_pensioengerechtigd_bij_leeftijd_op_datum(Geboortedatum, _, Pensioendatum),
    Restdagen #>= 1, % Er zijn nog restdagen in het verschil tussen vandaag en de pensioendatum
    datum_datum_maandenverschil_restdagen(Vandaag, Pensioendatum, 0, Restdagen). % verschil is 0 maanden
`

function str2datumObj(str) {
    const [jaar, maand, dag] = str.split("-")
    return {jaar, maand, dag}
}
function datumObj2natuurlijkeStr(datumObj) {
    return `${datumObj.dag}-${datumObj.maand}-${datumObj.jaar}`
}
function datumObj2prologStr(datumObj) {
    return `datum(${datumObj.jaar}, ${datumObj.maand}, ${datumObj.dag})`
}
export function calc(kwargs){
    const geboortedatum = str2datumObj(kwargs.geboortedatum)
    const vandaag = str2datumObj(kwargs.vandaag)
    const query1 = `
        % de datum waarop iemand die geboren is op ${datumObj2natuurlijkeStr(geboortedatum)} pensioengerechtigd wordt, en een
        % notificatie tekst wanneer dit al binnen een maand is op ${datumObj2natuurlijkeStr(vandaag)}:
        Geboortedatum = ${datumObj2prologStr(geboortedatum)},
        Vandaag = ${datumObj2prologStr(vandaag)},
        wordt_pensioengerechtigd_bij_leeftijd_op_datum(Geboortedatum, Pensioenleeftijd, Pensioendatum ),
        notificatie(Vandaag, Geboortedatum, DagenTotPensioen, Bericht).
    `
    console.log(query1)
    const firstSolution = swipl.prolog.query(query1).once()
    console.log(firstSolution)
    return firstSolution
}
var swipl
async function start() {
    swipl = await SWIPL({ arguments: ["-q"] })
    console.log("SWI-Prolog started")
    await swipl.prolog.consult("operators.pl")
    console.log("operators.pl loaded")
    await swipl.prolog.consult("artikel7a.pl")
    console.log("artikel7a.pl loaded")
    await swipl.prolog.load_string(notificatiePL)
    console.log("notificatiePL loaded")
}
export const loadingPromise = start()

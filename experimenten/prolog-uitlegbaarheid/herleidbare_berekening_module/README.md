## Run

ES6 modules kunnen niet geimporteerd worden in statische HTML pagina's, daarom is het nodig om een
simpele fileserver te draaien die de bestanden in deze map via localhost beschikbaar stelt.
Bijvoorbeeld met python:

```bash
python3 -m http.server
```

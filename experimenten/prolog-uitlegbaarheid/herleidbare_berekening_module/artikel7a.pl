% -*- Prolog -*- 
% Artikel 7a
%==================================================================================================================

% Artikel 7a
% ``De pensioengerechtigde leeftijd is:''

    % Artikel 7a 1 a.
    % ``vóór 1 januari 2013: 65 jaar;''
    de_pensioengerechtigde_leeftijd_op_datum(Leeftijd, Datum, ThisRef, ThisTree)
        :-
            datum_voor_datum(Datum, leaf(datum(2013, 1, 1), ref(art7a1a_id1, ["``1 januari 2013''"])),

                ref(art7a1a_id2, ["``vóór ...''"]), VoorTree
            ),

            Leeftijd = leaf(leeftijd(65, 0), ref(art7a1a_id3, ["``65 jaar''"])),

            ThisRef = ref(art7a1a_id4, [
                "``De pensioengerechtigde leeftijd is: ...''", 
                "``... : ...''"
            ]),

            ThisTree = node(
                fact("de_pensioengerechtigde_leeftijd_op_datum", ThisRef),
                children([Leeftijd, VoorTree]))
        .

    % Artikel 7a 1 b.
    % ``in 2013: 65 jaar en één maand;''
    de_pensioengerechtigde_leeftijd_op_datum(Leeftijd, Datum, ThisRef, ThisTree)
        :-
            datum_in_jaar(Datum, leaf(jaar(2013), ref(art7a1b_id1, ["``2013''"])),

                ref(art7a1b_id2, ["``in ...''"]), InTree
            ),

            Leeftijd = leaf(leeftijd(65, 1), ref(art7a1b_id3, ["``65 jaar en één maand''"])),

            ThisRef = ref(art7a1b_id4, [
                "``De pensioengerechtigde leeftijd is: ...''",
                "`` ; ''",
                "``... : ...''"
            ]),

            ThisTree = node(
                fact("de_pensioengerechtigde_leeftijd_op_datum", ThisRef),
                children([Leeftijd, InTree]))
        .

    % Artikel 7a 1 c.
    % ``in 2014: 65 jaar en twee maanden;''
    de_pensioengerechtigde_leeftijd_op_datum(Leeftijd, Datum, ThisRef, ThisTree)
        :-
            datum_in_jaar(Datum, leaf(jaar(2014), ref(art7a1c_id1, ["``2014''"])),

                ref(art7a1c_id2, ["``in ...''"]), InTree
            ),

            Leeftijd = leaf(leeftijd(65, 2), ref(art7a1c_id3, ["``65 jaar en twee maanden''"])),

            ThisRef = ref(art7a1c_id4, [
                "``De pensioengerechtigde leeftijd is: ...''",
                "`` ; ''",
                "`` ; ''",
                "``... : ...''"
            ]),

            ThisTree = node(
                fact("de_pensioengerechtigde_leeftijd_op_datum", ThisRef),
                children([Leeftijd, InTree]))
        .

    % Artikel 7a 1 d.
    % ``in 2015: 65 jaar en drie maanden;''
    de_pensioengerechtigde_leeftijd_op_datum(Leeftijd, Datum, ThisRef, ThisTree)
        :-
            datum_in_jaar(Datum, leaf(jaar(2015), ref(art7a1d_id1, ["``2015''"])),

                ref(art7a1d_id2, ["``in ...''"]), InTree
            ),

            Leeftijd = leaf(leeftijd(65, 3), ref(art7a1d_id3, ["``65 jaar en drie maanden''"])),

            ThisRef = ref(art7a1d_id4, [
                "``De pensioengerechtigde leeftijd is: ...''",
                "`` ; ''",
                "`` ; ''",
                "`` ; ''",
                "``... : ...''"
            ]),

            ThisTree = node(
                fact("de_pensioengerechtigde_leeftijd_op_datum", ThisRef),
                children([Leeftijd, InTree]))
        .

    % Artikel 7a 1 e.
    % ``in 2016: 65 jaar en zes maanden;''
    de_pensioengerechtigde_leeftijd_op_datum(Leeftijd, Datum, ThisRef, ThisTree)
        :-
            datum_in_jaar(Datum, leaf(jaar(2016), ref(art7a1e_id1, ["``2016''"])),

                ref(art7a1e_id2, ["``in ...''"]), InTree
            ),

            Leeftijd = leaf(leeftijd(65, 6), ref(art7a1e_id3, ["``65 jaar en zes maanden''"])),

            ThisRef = ref(art7a1e_id4, [
                "``De pensioengerechtigde leeftijd is: ...''",
                "`` ; ''",
                "`` ; ''",
                "`` ; ''",
                "`` ; ''",
                "``... : ...''"
            ]),

            ThisTree = node(
                fact("de_pensioengerechtigde_leeftijd_op_datum", ThisRef),
                children([Leeftijd, InTree]))
        .

    % Artikel 7a 1 f.
    % ``in 2017: 65 jaar en negen maanden;''
    de_pensioengerechtigde_leeftijd_op_datum(Leeftijd, Datum, ThisRef, ThisTree)
        :-
            datum_in_jaar(Datum, leaf(jaar(2017), ref(art7a1f_id1, ["``2017''"])),

                ref(art7a1f_id2, ["``in ...''"]), InTree
            ),

            Leeftijd = leaf(leeftijd(65, 9), ref(art7a1f_id3, ["``65 jaar en negen maanden''"])),

            ThisRef = ref(art7a1f_id4, [
                "``De pensioengerechtigde leeftijd is: ...''",
                "`` ; ''",
                "`` ; ''",
                "`` ; ''",
                "`` ; ''",
                "`` ; ''",
                "``... : ...''"
            ]),

            ThisTree = node(
                fact("de_pensioengerechtigde_leeftijd_op_datum", ThisRef),
                children([Leeftijd, InTree])
            )
        .

    % Artikel 7a 1 g.
    % ``in 2018: 66 jaar;''
    de_pensioengerechtigde_leeftijd_op_datum(Leeftijd, Datum, ThisRef, ThisTree)
        :-
            datum_in_jaar(Datum, leaf(jaar(2018), ref(art7a1g_id1, ["``2018''"])),

                ref(art7a1g_id2, ["``in ...''"]), InTree
            ),

            Leeftijd = leaf(leeftijd(66, 0), ref(art7a1g_id3, ["``66 jaar''"])),

            ThisRef = ref(art7a1g_id4, [
                "``De pensioengerechtigde leeftijd is: ...''",
                "`` ; ''",
                "`` ; ''",
                "`` ; ''",
                "`` ; ''",
                "`` ; ''",
                "`` ; ''",
                "``... : ...''"
            ]),

            ThisTree = node(
                fact("de_pensioengerechtigde_leeftijd_op_datum", ThisRef),
                children([Leeftijd, InTree])
            )
        .

    % Artikel 7a 1 h.
    % ``in 2019: 66 jaar en vier maanden;''
    de_pensioengerechtigde_leeftijd_op_datum(Leeftijd, Datum, ThisRef, ThisTree)
        :-
            datum_in_jaar(Datum, leaf(jaar(2019), ref(art7a1h_id1, ["``2019''"])),

                ref(art7a1h_id2, ["``in ...''"]), InTree
            ),

            Leeftijd = leaf(leeftijd(66, 4), ref(art7a1h_id3, ["``66 jaar en vier maanden''"])),

            ThisRef = ref(art7a1h_id4, [
                "``De pensioengerechtigde leeftijd is: ...''",
                "`` ; ''",
                "`` ; ''",
                "`` ; ''",
                "`` ; ''",
                "`` ; ''",
                "`` ; ''",
                "`` ; ''",
                "``... : ...''"
            ]),

            ThisTree = node(
                fact("de_pensioengerechtigde_leeftijd_op_datum", ThisRef),
                children([Leeftijd, InTree])
            )
        .

    % Artikel 7a 1 i.
    % ``in 2020: 66 jaar en vier maanden;''
    de_pensioengerechtigde_leeftijd_op_datum(Leeftijd, Datum, ThisRef, ThisTree)
        :-
            datum_in_jaar(Datum, leaf(jaar(2020), ref(art7a1i_id1, ["``2020''"])),

                ref(art7a1i_id2, ["``in ...''"]), InTree
            ),

            Leeftijd = leaf(leeftijd(66, 4), ref(art7a1i_id3, ["``66 jaar en vier maanden''"])),
            ThisRef = ref(art7a1i_id4, [
                "``De pensioengerechtigde leeftijd is: ...''",
                "`` ; ''",
                "`` ; ''",
                "`` ; ''",
                "`` ; ''",
                "`` ; ''",
                "`` ; ''",
                "`` ; ''",
                "`` ; ''",
                "``... : ...''"
            ]),
            ThisTree = node(
                fact("de_pensioengerechtigde_leeftijd_op_datum", ThisRef),
                children([Leeftijd, InTree])
            )
        .

    % Artikel 7a 1 j.
    % ``in 2021: 66 jaar en vier maanden;''
    de_pensioengerechtigde_leeftijd_op_datum(Leeftijd, Datum, ThisRef, ThisTree)
        :-
            datum_in_jaar(Datum, leaf(jaar(2021), ref(art7a1j_id1, ["``2021''"])),

                ref(art7a1j_id2, ["``in ...''"]), InTree
            ),

            Leeftijd = leaf(leeftijd(66, 4), ref(art7a1j_id3, ["``66 jaar en vier maanden''"])),

            ThisRef = ref(art7a1j_id4, [
                "``De pensioengerechtigde leeftijd is: ...''",
                "`` ; ''",
                "`` ; ''",
                "`` ; ''",
                "`` ; ''",
                "`` ; ''",
                "`` ; ''",
                "`` ; ''",
                "`` ; ''",
                "`` ; ''",
                "``... : ...''"
            ]),

            ThisTree = node(
                fact("de_pensioengerechtigde_leeftijd_op_datum", ThisRef),
                children([Leeftijd, InTree])
            )
        .

    % Artikel 7a 1 k.
    % ``in 2022: 66 jaar en zeven maanden;''
    de_pensioengerechtigde_leeftijd_op_datum(Leeftijd, Datum, ThisRef, ThisTree)
        :-
            datum_in_jaar(Datum, leaf(jaar(2022), ref(art7a1k_id1, ["``2022''"])),

                ref(art7a1k_id2, ["``in ...''"]), InTree
            ),

            Leeftijd = leaf(leeftijd(66, 7), ref(art7a1k_id3, ["``66 jaar en zeven maanden''"])),
    
            ThisRef = ref(art7a1k_id4, [
                "``De pensioengerechtigde leeftijd is: ...''",
                "`` ; ''",
                "`` ; ''",
                "`` ; ''",
                "`` ; ''",
                "`` ; ''",
                "`` ; ''",
                "`` ; ''",
                "`` ; ''",
                "`` ; ''",
                "`` ; ''",
                "``... : ...''"
            ]),

            ThisTree = node(
                fact("de_pensioengerechtigde_leeftijd_op_datum", ThisRef),
                children([Leeftijd, InTree]))
        .

    % Artikel 7a 1 l.
    % ``in 2023: 66 jaar en tien maanden;''
    de_pensioengerechtigde_leeftijd_op_datum(Leeftijd, Datum, ThisRef, ThisTree)
        :-
            datum_in_jaar(Datum, leaf(jaar(2023), ref(art7a1l_id1, ["``2023''"])),

                ref(art7a1l_id2, ["``in ...''"]), InTree
            ),

            Leeftijd = leaf(leeftijd(66, 10), ref(art7a1l_id3, ["``66 jaar en tien maanden''"])),
            ThisRef = ref(art7a1l_id4, [
                "``De pensioengerechtigde leeftijd is: ...''",
                "`` ; ''",
                "`` ; ''",
                "`` ; ''",
                "`` ; ''",
                "`` ; ''",
                "`` ; ''",
                "`` ; ''",
                "`` ; ''",
                "`` ; ''",
                "`` ; ''",
                "`` ; ''",
                "``... : ...''"
            ]),
            ThisTree = node(
                fact("de_pensioengerechtigde_leeftijd_op_datum", ThisRef),
                children([Leeftijd, InTree]))
        .

    % Artikel 7a 1 m.
    % ``in 2024: 67 jaar;''
    de_pensioengerechtigde_leeftijd_op_datum(Leeftijd, Datum, ThisRef, ThisTree)
        :-
            datum_in_jaar(Datum, leaf(jaar(2024), ref(art7a1m_id1, ["``2024''"])),

                ref(art7a1m_id2, ["``in ...''"]), InTree
            ),

            Leeftijd = leaf(leeftijd(67, 0), ref(art7a1m_id3, ["``67 jaar''"])),

            ThisRef = ref(art7a1m_id4, [
                "``De pensioengerechtigde leeftijd is: ...''",
                "`` ; ''",
                "`` ; ''",
                "`` ; ''",
                "`` ; ''",
                "`` ; ''",
                "`` ; ''",
                "`` ; ''",
                "`` ; ''",
                "`` ; ''",
                "`` ; ''",
                "`` ; ''",
                "`` ; ''",
                "``... : ...''"
            ]),

            ThisTree = node(
                fact("de_pensioengerechtigde_leeftijd_op_datum", ThisRef),
                children([Leeftijd, InTree]))
        .

    % Artikel 7a 1 n.
    % ``in 2025: 67 jaar;''
    de_pensioengerechtigde_leeftijd_op_datum(Leeftijd, Datum, ThisRef, ThisTree)
        :-
            datum_in_jaar(Datum, leaf(jaar(2025), ref(art7a1n_id1, ["``2025''"])),

                ref(art7a1n_id2, ["``in ...''"]), InTree
            ),

            Leeftijd = leaf(leeftijd(67, 0), ref(art7a1n_id3, ["``67 jaar''"])),

            ThisRef = ref(art7a1n_id4, [
                "``De pensioengerechtigde leeftijd is: ...''",
                "`` ; ''",
                "`` ; ''",
                "`` ; ''",
                "`` ; ''",
                "`` ; ''",
                "`` ; ''",
                "`` ; ''",
                "`` ; ''",
                "`` ; ''",
                "`` ; ''",
                "`` ; ''",
                "`` ; ''",
                "`` ; ''",
                "``... : ...''"
            ]),

            ThisTree = node(
                fact("de_pensioengerechtigde_leeftijd_op_datum", ThisRef),
                children([Leeftijd, InTree]))
        .

    % Artikel 7a 1 o.
    % ``in 2026 en de kalenderjaren daarna: de pensioengerechtigde leeftijd, die jaarlijks
    % op basis van de verhoging van de pensioengerechtigde leeftijd op grond van het
    % tweede lid wordt vastgesteld.''
    de_pensioengerechtigde_leeftijd_op_datum(Leeftijd, Datum, ThisRef, ThisTree)
        :-
            datum_in_of_na_jaar(Datum, leaf(jaar(2026), ref(art7a1o_id1, ["``2026''"])),

                ref(art7a1o_id2, ["``in ...''", "``... en de kalenderjaren daarna''"]), InTree
            ),

            Leeftijd = leaf(leeftijd_nog_niet_vastgesteld,
                ref(art7a1o_id3, [
                    "``de pensioengerechtigde leeftijd, die jaarlijks op basis van de verhoging van de pensioengerechtigde leeftijd op grond van het tweede lid wordt vastgesteld''"
                ])
            ),

            ThisRef = ref(art7a1o_id4, [
                "``De pensioengerechtigde leeftijd is: ...''",
                "`` ; ''",
                "`` ; ''",
                "`` ; ''",
                "`` ; ''",
                "`` ; ''",
                "`` ; ''",
                "`` ; ''",
                "`` ; ''",
                "`` ; ''",
                "`` ; ''",
                "`` ; ''",
                "`` ; ''",
                "`` ; ''",
                "`` ; ''",
                "``... : ...''",
                "``... . ''"
            ]),

            ThisTree = node(
                fact("de_pensioengerechtigde_leeftijd_op_datum", ThisRef),
                children([Leeftijd, InTree]))
        .

% de aanwezigheid van de tabel in Artikel 7a 1 geeft aan dat iemand een pensioengerechtigde leeftijd
% bereikt wanneer deze gelijk is aan de eigen leeftijd op een moment dat de pensioengerechtigde
% leeftijd van kracht is
bereikt_de_pensioengerechtigde_leeftijd_op_datum(Geboortedatum, Leeftijd, Datum, ThisRef, ThisTree)
    :-
        bereikt_leeftijd_op_datum(Geboortedatum, Leeftijd, Datum,

            ref(art7a1_id1, [
                "``... leeftijd hebben bereikt ...''"
            ]), LeeftijdBereiktTree
        ),

        de_pensioengerechtigde_leeftijd_op_datum(Leeftijd, Datum, _, PensioenleeftijdTree),

        ThisTree = node(
            operator("bereikt_de_pensioengerechtigde_leeftijd_op_datum", ThisRef),
            children([LeeftijdBereiktTree, PensioenleeftijdTree]))
    .


% Artikel 7a 1
% ``Op pensioengerechtigden die ...''
    % ... in een bepaald kalenderjaar de pensioengerechtigde leeftijd hebben bereikt ...''
    bereikt_pensioengerechtigde_leeftijd_in_kalenderjaar(Geboortedatum, Kalenderjaar, ThisTree)
        :-
            datum_in_jaar(Datum, Kalenderjaar,

                ref(art7a1_id2, [
                    "``... in een bepaald kalenderjaar ...``"
                ]), InJaarTree
            ),

            bereikt_de_pensioengerechtigde_leeftijd_op_datum(Geboortedatum, _, Datum,

                ref(art7a1_id3, [
                    "``... de pensioengerechtigde leeftijd hebben bereikt ...''"
                ]), PensioenLeeftijdBereiktTree
            ),

            ThisTree = node(
                fact("bereikt_pensioengerechtigde_leeftijd_in_kalenderjaar", 
                    ref(art7a1_id4, [
                        "``... in een bepaald kalenderjaar de pensioengerechtigde leeftijd hebben bereikt ...''"
                    ])),
                children([InJaarTree, PensioenLeeftijdBereiktTree]))
        .

    % ``... is de pensioengerechtigde leeftijd in de kalenderjaren daarna ...''
    bereikt_pensioengerechtigde_leeftijd_in_een_eerder_kalenderjaar(Geboortedatum, Kalenderjaar, ThisTree)
        :-
            % ``bepaald kalenderjaar'' is eerder dan Kalenderjaar want er wordt gesproken over ``kalenderjaren daarna''
            bereikt_pensioengerechtigde_leeftijd_in_kalenderjaar(Geboortedatum, BepaaldKalenderjaar, 
                BepaaldKalenderjaarTree),

            BepaaldKalenderjaar = leaf(jaar(BepaaldKalenderjaarInt), _),
            Kalenderjaar = leaf(jaar(KalenderjaarInt), _),
            
            BepaaldKalenderjaarInt #< KalenderjaarInt,

            ThisTree = node(
                fact("bereikt_pensioengerechtigde_leeftijd_in_een_eerder_kalenderjaar", 
                    ref(art7a1_id5, [
                        "``... is de pensioengerechtigde leeftijd in de kalenderjaren daarna ...''"
                ])),
                children([Kalenderjaar, BepaaldKalenderjaarTree]))
        .
    % ``... niet van toepassing.''
    geboortedatum_waarbij_de_pensioengerechtigde_leeftijd_in_kalenderjaar_NIET_van_toepassing_is(Geboortedatum, Kalenderjaar, ThisTree)
        :-
            bereikt_pensioengerechtigde_leeftijd_in_een_eerder_kalenderjaar(Geboortedatum, Kalenderjaar, 
                BereiktInEerderKalenderjaarTree),

            ThisTree = node(
                fact("geboortedatum_waarbij_de_pensioengerechtigde_leeftijd_in_kalenderjaar_NIET_van_toepassing_is", 
                    ref(art7a1_id6, [
                        "``Op pensioengerechtigden die ...''",
                        "``... niet van toepassing.''"
                ])),
                children([BereiktInEerderKalenderjaarTree]))
        .

    % de pensioengerechtigde leeftijd moet logischerwijs WEL van toepassing zijn wanneer niet kan
    % worden vastgesteld (de \+ operator) dat het NIET van toepassing is
    geboortedatum_waarbij_de_pensioengerechtigde_leeftijd_in_kalenderjaar_WEL_van_toepassing_is(Geboortedatum, Kalenderjaar, ThisTree)
        :-
            \+ geboortedatum_waarbij_de_pensioengerechtigde_leeftijd_in_kalenderjaar_NIET_van_toepassing_is(
                    Geboortedatum, Kalenderjaar, _),

            ThisTree = node(
                fact("geboortedatum_waarbij_de_pensioengerechtigde_leeftijd_in_kalenderjaar_WEL_van_toepassing_is", 
                    interpretation(art7a1_id7, [
                        "``Op pensioengerechtigden die ...''",
                        "*de pensioengerechtigde leeftijd is van toepassing wanneer niet kan worden vastgesteld dat de pensioengerechtigde leeftijd niet van toepassing is*"
                ])),
                children([]))
        .

wordt_pensioengerechtigd_bij_leeftijd_op_datum(Geboortedatum, Pensioenleeftijd, Pensioendatum, ThisTree)
    :-
        Geboortedatum = leaf(datum(_,_,_), _),
        Pensioenleeftijd = leaf(leeftijd(_,_), _),
        Pensioendatum = leaf(datum(_,_,_), _),

        bereikt_de_pensioengerechtigde_leeftijd_op_datum(Geboortedatum, Pensioenleeftijd, Pensioendatum,

            interpretation(art7a1_id8, [
                "*het bereiken van de pensioengerechtigde leeftijd is nodig om pensioengerechtigd te worden*"
            ]), PensioenLeeftijdBereiktTree),

        datum_in_jaar(Pensioendatum, PensioenKalenderjaar,

            interpretation(art7a1_id9, [
                "*kalenderjaar om de pensioengerechtigde leeftijd vast te kunnen stellen*"
            ]), InJaarTree),

        geboortedatum_waarbij_de_pensioengerechtigde_leeftijd_in_kalenderjaar_WEL_van_toepassing_is(
            Geboortedatum, PensioenKalenderjaar, VanToepassingTree),

        ThisTree = node(
            fact("wordt_pensioengerechtigd_bij_leeftijd_op_datum",
                interpretation(art7a1_id10, [
                    "*een persoon wordt pensioengerechtigd bij de eerste keer dat een pensioengerechtigde leeftijd wordt bereikt in een bijbehorend kalenderjaar*"
                ])),
            children([PensioenLeeftijdBereiktTree, InJaarTree, VanToepassingTree])
        )
    .


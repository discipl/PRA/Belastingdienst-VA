% -*- Prolog -*- 
% Herleidbare operaties
%==================================================================================================================

datum_in_jaar(
            leaf(Datum, SubRef1),
            leaf(jaar(Jaar), SubRef2),
            Ref, node(
                operator("datum_in_jaar", Ref),
                children([
                    leaf(Datum, SubRef1),
                    leaf(jaar(Jaar), SubRef2)
                ])))
    :- datum_in_jaar(Datum, Jaar) .

datum_in_of_na_jaar(
            leaf(Datum, SubRef1),
            leaf(jaar(Jaar), SubRef2),
            Ref, node(
                operator("datum_in_of_na_jaar", Ref),
                children([
                    leaf(Datum, SubRef1),
                    leaf(jaar(Jaar), SubRef2)
                ])))
    :- datum_in_of_na_jaar(Datum, Jaar) .

datum_voor_datum(
            leaf(Datum1, SubRef1),
            leaf(Datum2, SubRef2),
            Ref, node(
                operator("datum_voor_datum", Ref),
                children([
                    leaf(Datum1, SubRef1),
                    leaf(Datum2, SubRef2)
                ])))
    :- datum_voor_datum(Datum1, Datum2) .

datum_na_datum(
            leaf(Datum1, SubRef1),
            leaf(Datum2, SubRef2),
            Ref, node(
                operator("datum_na_datum", Ref),
                children([
                    leaf(Datum1, SubRef1),
                    leaf(Datum2, SubRef2)
                ])))
    :- datum_na_datum(Datum1, Datum2) .

bereikt_leeftijd_op_datum(
            leaf(Geboortedatum, SubRef1),
            leaf(Leeftijd, SubRef2),
            leaf(Datum, SubRef3),
            Ref, node(
                operator("bereikt_leeftijd_op_datum", Ref),
                children([
                    leaf(Geboortedatum, SubRef1),
                    leaf(Leeftijd, SubRef2),
                    leaf(Datum, SubRef3)
                ])))
    :- bereikt_leeftijd_op_datum(Geboortedatum, Leeftijd, Datum) .


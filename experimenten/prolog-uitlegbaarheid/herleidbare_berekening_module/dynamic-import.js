import { SWIPL } from "https://SWI-Prolog.github.io/npm-swipl-wasm/3/7/8/dynamic-import.js";

function str2datumObj(str) {
    const [jaar, maand, dag] = str.split("-")
    return {jaar, maand, dag}
}
function datumObj2natuurlijkeStr(datumObj) {
    return `${datumObj.dag}-${datumObj.maand}-${datumObj.jaar}`
}
function datumObj2prologStr(datumObj) {
    return `datum(${datumObj.jaar}, ${datumObj.maand}, ${datumObj.dag})`
}
export function calc(kwargs){
    const geboortedatum = str2datumObj(kwargs.geboortedatum)
    const query1 = `
        % dit vindt de datum waarop iemand die geboren is op ${datumObj2natuurlijkeStr(geboortedatum)} pensioengerechtigd wordt:
        Geboortedatum = leaf(${datumObj2prologStr(geboortedatum)}, ref(wallet_id1, ["geboortedatum"])),
        Pensioendatum = leaf(_, ref(pra_id1_pensioendatum, ["om de gebruiker de datum te tonen vanaf wanneer degene pensioengerechtigd is"])),
        wordt_pensioengerechtigd_bij_leeftijd_op_datum(Geboortedatum, Pensioenleeftijd, Pensioendatum, Herleidbaarheid).
    `
    console.log(query1)
    const firstSolution = swipl.prolog.query(query1).once()
    console.log(firstSolution)
    return firstSolution
}
var swipl
async function start() {
    swipl = await SWIPL({ arguments: ["-q"] })
    console.log("SWI-Prolog started")
    await swipl.prolog.consult("operators.pl")
    console.log("operators.pl loaded")
    await swipl.prolog.consult("herleidbarde_operators.pl")
    console.log("herleidbarde_operators.pl loaded")
    await swipl.prolog.consult("artikel7a.pl")
    console.log("artikel7a.pl loaded")
}
export const loadingPromise = start()

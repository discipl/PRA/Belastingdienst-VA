import math
import json
import pprint 
pp = pprint.PrettyPrinter(indent=2)

# TODO eerste tarief hangt eigenlijk af van de maand waarin iemand de
#      AOW-leeftijd berijkt in 2023
#      https://www.belastingdienst.nl/wps/wcm/connect/bldcontentnl/belastingdienst/prive/inkomstenbelasting/heffingskortingen_boxen_tarieven/boxen_en_tarieven/overzicht_tarieven_en_schijven/u-hebt-in-2023-aow-leeftijd
class InkomstenbelastingPensioen2023:
    def __init__(self):
        self.tarieven = [.1903, .3693, .4950]
        self.inkomensgrenzen = [37_149, 73_031]
        self.ouderenkortinggrens = 49324
        self.ouderenkorting = 1835
        self.alleenstaande_ouderenkorting = 478
    
    def loonheffing(self, bruto_loon):
        verrekend = 0
        belasting_per_schijf = []
        # TODO verrekend_per_schijf wordt alleen gebruikt voor het printen (om
        #      de berekening te kunnen controleren) dit kan misschien weg?
        verrekend_per_schijf = [verrekend]
        for tarief, inkomensgrens in zip(self.tarieven, self.inkomensgrenzen):
            loon_in_schijf = math.ceil(min(inkomensgrens, bruto_loon)-verrekend)
            belasting_per_schijf.append(math.floor(tarief*loon_in_schijf))
            verrekend += loon_in_schijf
            verrekend_per_schijf.append(verrekend-verrekend_per_schijf[-1])
        
        belasting_per_schijf.append(math.floor(self.tarieven[-1]*(bruto_loon-verrekend)))
        verrekend_per_schijf.append(bruto_loon-verrekend)
        print(f"{bruto_loon} is als volgt verdeeld over de belastingschijven: {verrekend_per_schijf[1:]}. Dit resulteert in {belasting_per_schijf} belasting per schijf.")
        return belasting_per_schijf 

    def ingehouden_aowbelasting(self, bruto, alleenstaand):
        ingehouden = sum(self.loonheffing(bruto)) 
        if bruto < self.ouderenkortinggrens:
            print(f'Ouderenkorting van {self.ouderenkorting} is gegeven')
            ingehouden -= self.ouderenkorting
            if alleenstaand:
                print(f'De extra alleenstaande ouderenkorting van {self.alleenstaande_ouderenkorting} is ook gegeven.')
                ingehouden -= self.alleenstaande_ouderenkorting
        return ingehouden

    def ingehouden_pensioenbelasting(self, bruto):
        ingehouden = sum(self.loonheffing(bruto)) 
        return ingehouden


    def bereken_va(self, persoon):
        ingehouden= {}
        print('AOW')
        ingehouden['AOW'] = self.ingehouden_aowbelasting(persoon.aow, alleenstaand=persoon.alleenstaand)
        print()
        for uitvoerder, bedrag in persoon.potjes.items():
            print(uitvoerder)
            ingehouden[uitvoerder] = self.ingehouden_pensioenbelasting(bedrag)
            print()
        print("Per potje zijn de volgende bedragen ingehouden:")
        pp.pprint(ingehouden)
        print()
        return sum(ingehouden.values())
    
    def bereken_werkelijke_aanslag(self, persoon):
        print('Totaal pensioen')
        werkelijke_loonheffing = self.loonheffing(persoon.aow + sum(persoon.potjes.values()))
        print()
        return sum(werkelijke_loonheffing)
        
    def correctie_pensioenaanslag(self, persoon):

        voorlopige_aanslag = self.bereken_va(persoon)
        print("Totaal ingehouden volgens VA:", voorlopige_aanslag)
        print()
        werkelijke_aanslag = self.bereken_werkelijke_aanslag(persoon)
        print("Daadwerkelijk in te houden:", werkelijke_aanslag)
        correctie = werkelijke_aanslag - voorlopige_aanslag
        print("Te betalen aan de belastingdienst:", correctie)
        return correctie


class AOWGerechtigde:
    def __init__(self, mijnpensioenoverzicht_json_path, alleenstaand=True):
        with open(mijnpensioenoverzicht_json_path, 'r') as f:
            self.mijnpensioenoverzicht = json.load(f)
        self.alleenstaand = alleenstaand
        self._laatste_pensioen = self._get_laatste_pensioen()
        self.aow = self._get_aow()
        self.potjes = self._get_potjes()

    def _get_laatste_pensioen(self):
        pensioenperiodes = self.mijnpensioenoverzicht['Details']['OuderdomsPensioenDetails']['OuderdomsPensioen']
        return [x for x in pensioenperiodes if x['Tot'] == {'OuderdomsPensioenEvent': 'Overlijden'}][0]

    def _get_aow(self):
        if self.alleenstaand:
            return self._laatste_pensioen['AOW']['AOWDetailsOpbouw']['TeBereikenAlleenstaand']
        else:
            return self._laatste_pensioen['AOW']['AOWDetailsOpbouw']['TeBereikenSamenwonend']

    def _get_potjes(self):
        potjes = {}
        for potje in self._laatste_pensioen['Pensioen']:
            uitvoerder = potje['PensioenUitvoerder']
            potjes[uitvoerder] = potjes.get(uitvoerder, 0) + potje['TeBereiken']
        return potjes
    
    def __repr__(self):
        return f"AOWGerechtigde({self.alleenstaand}, {self.aow}, {self.potjes})"


inkomstenbelasting_pensioen_2023 = InkomstenbelastingPensioen2023()

print('\n----------------------\n')

print("a.d.h.v mijnpensioenoverzicht.json als alleenstaande")
print()
voorbeeldpersoon = AOWGerechtigde("mijnpensioenoverzicht.json", alleenstaand=True)
print(voorbeeldpersoon)
inkomstenbelasting_pensioen_2023.correctie_pensioenaanslag(voorbeeldpersoon)

print('\n----------------------\n')

print("a.d.h.v mijnpensioenoverzicht.json met partner")
print()
voorbeeldpersoon = AOWGerechtigde("mijnpensioenoverzicht.json", alleenstaand=False)
print(voorbeeldpersoon)
inkomstenbelasting_pensioen_2023.correctie_pensioenaanslag(voorbeeldpersoon)

print('\n----------------------\n')

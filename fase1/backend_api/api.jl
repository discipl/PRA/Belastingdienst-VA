using Genie, Genie.Router, Genie.Renderer.Json, Genie.Requests

#using PythonCall
#const inkomstenbelasting_pensioen_2023 = pyimport("python_src.inkomstenbelasting_pensioen_2023")
#const InkomstenbelastingPensioen2023 = inkomstenbelasting_pensioen_2023.InkomstenbelastingPensioen2023()
#InkomstenbelastingPensioen2023

include("inkomstenbelasting_pensioen.jl")

route("/correctie_pensioenaanslag/2023", method="POST") do
    payload = jsonpayload()
    alleenstaand = get(payload, "alleenstaand", true)
    aow = get(payload, "aow", 0)
    potjes = get(payload, "potjes", [0])
    persoon = AOWGerechtigde(alleenstaand, aow, potjes)
    json(correctie_pensioenaanslag(InkomstenbelastingPensioen2023(), persoon))
end

up(async=false)

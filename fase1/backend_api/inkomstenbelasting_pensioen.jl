struct InkomstenbelastingPensioen2023 end

tarieven(::InkomstenbelastingPensioen2023) = [.1903, .3693, .4950]
inkomensgrenzen(::InkomstenbelastingPensioen2023) = [37_149, 73_031]
ouderenkortinggrens(::InkomstenbelastingPensioen2023) = 49324
ouderenkorting(::InkomstenbelastingPensioen2023) = 1835
alleenstaande_ouderenkorting(::InkomstenbelastingPensioen2023) = 478

struct AOWGerechtigde
    alleenstaand::Bool
    aow::Int
    potjes::Vector{Int}
end

alleenstaand(x::AOWGerechtigde) = x.alleenstaand
aow(x::AOWGerechtigde) = x.aow
potjes(x::AOWGerechtigde) = x.potjes

function loonheffing(x::InkomstenbelastingPensioen2023, brutoloon::Int)
    verrekend = 0
    belasting_per_schijf = []
    for (tarief, inkomensgrens) in zip(tarieven(x), inkomensgrenzen(x))
        loon_in_schijf = ceil(Int, min(inkomensgrens, brutoloon)-verrekend)
        push!(belasting_per_schijf, floor(Int, tarief*loon_in_schijf))
        verrekend += loon_in_schijf
    end
    push!(belasting_per_schijf, floor(Int, tarieven(x)[end]*(brutoloon-verrekend)))
    belasting_per_schijf 
end

ingehouden_aowbelasting(x::InkomstenbelastingPensioen2023, p::AOWGerechtigde) =
    ingehouden_aowbelasting(x, aow(p), alleenstaand(p))

function ingehouden_aowbelasting(x::InkomstenbelastingPensioen2023, brutoloon::Int, alleenstaand::Bool)
    ingehouden = sum(loonheffing(x, brutoloon))
    if brutoloon < ouderenkortinggrens(x)
        ingehouden -= ouderenkorting(x)
        if alleenstaand 
            ingehouden -= alleenstaande_ouderenkorting(x)
        end
    end
    ingehouden
end

ingehouden_pensioenbelasting(x::InkomstenbelastingPensioen2023, brutoloon::Int) = 
    sum(loonheffing(x, brutoloon)) 

bereken_va(x::InkomstenbelastingPensioen2023, persoon::AOWGerechtigde) =
    ingehouden_aowbelasting(x, persoon) + sum(ingehouden_pensioenbelasting.(Ref(x), potjes(persoon)))

bereken_werkelijke_aanslag(x::InkomstenbelastingPensioen2023, persoon::AOWGerechtigde) =
    sum(loonheffing(x, aow(persoon) + sum(potjes(persoon))))

function correctie_pensioenaanslag(x::InkomstenbelastingPensioen2023, persoon::AOWGerechtigde)
    r = Dict{String, Int}()
    voorlopige_aanslag = bereken_va(x, persoon)
    r["ingehouden"] = voorlopige_aanslag
    println("Totaal ingehouden volgens VA:", voorlopige_aanslag)
    println()
    werkelijke_aanslag = bereken_werkelijke_aanslag(x, persoon)
    r["totaal_in_te_houden"] = werkelijke_aanslag
    println("Daadwerkelijk in te houden:", werkelijke_aanslag)
    correctie = werkelijke_aanslag - voorlopige_aanslag
    r["nog_te_betalen"] = correctie
    println("Te betalen aan de belastingdienst:", correctie)
    r
end

#voorbeeldpersoon1 = AOWGerechtigde(true, 18031, [11140, 49655, 0])
#voorbeeldpersoon2 = AOWGerechtigde(false, 12301, [11140, 49655, 0])
#
#correctie_pensioenaanslag(InkomstenbelastingPensioen2023(), voorbeeldpersoon1)
#correctie_pensioenaanslag(InkomstenbelastingPensioen2023(), voorbeeldpersoon2)

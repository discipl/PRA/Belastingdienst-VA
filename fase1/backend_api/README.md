
## Setting up environment 

```bash
cd backend_api/
julia --project=.
```

```julia
using Pkg
Pkg.instantiate()
```

## Run backend server

```bash
julia --project api.jl
```

## Usage example

```bash
curl -X POST http://localhost:8000/correctie_pensioenaanslag/2023    -H 'Content-Type: application/json'    -d '{"alleenstaand":true, "aow": 18031, "potjes": [11140,49655,0]}'
```

Returns:

```json
{"nog_te_betalen":8264,"ingehouden":14924,"totaal_in_te_houden":23188}
```

```bash
curl -X POST http://localhost:8000/correctie_pensioenaanslag/2023    -H 'Content-Type: application/json'    -d '{"alleenstaand":false, "aow": 12301, "potjes": [11140,49655,0]}'
```

Returns:

```json
{"nog_te_betalen":6041,"ingehouden":14311,"totaal_in_te_houden":20352}
```

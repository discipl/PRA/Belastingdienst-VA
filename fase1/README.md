## Description

This folder contains files used for prototyping the PRA specifically in a first phase of the project for the Dutch Tax Administration (Belastingdienst) on the topic of a specific case in which some people are confronted with high amounts to be paid after a first year since they retired.

 - mijnpensioenoverzicht.json : example data export of mijnpensioenoverzicht for the Persona used within this project (fictional data)
 - voorlopige_aanslag.json : fictional data export of a VA (voorlopige aanslag) as could be issued by the Belastingdienst to the end user's wallet
 - inkomstenbelasting_pensioen_2023.py : implementation of simplified income tax 2023 calculation (for prototyping and demonstration purpose only)
 - backend_api : all files for a serverside API for simplified income tax calculations (for prototyping and demonstration purpose only)
 - yivi : all files used for demonstrating the Yivi wallet in combination with the PRA 

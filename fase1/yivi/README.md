
## Setting up environment 

### Python environment

This is not needed when using julia

```bash
python -m venv .venv
source .venv/bin/activate
python -m pip install -e ./parse2yivi/
pip install requests
```

### Julia environment

```bash
cd issuer_ui/
julia --project
```

```julia
using Pkg
Pkg.instantiate()
using CondaPkg
CondaPkg.PkgREPL.run(`python -m pip install -e ../parse2yivi/`)
```

(or using the Pkg-REPL instead)

```julia
]conda run python -m pip install -e ../parse2yivi/
```

## Run

```bash
julia --project app2.jl
```

```bash
irma server --sse
```

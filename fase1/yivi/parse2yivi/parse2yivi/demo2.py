import itertools

from . import common

import json

def make_OuderdomsPensioenPeriode(OuderdomsPensioen):
    (van, tot), details = OuderdomsPensioen
    return {
        "van": van,
        "tot": tot,
        "aantal": len(details),
        "teBereikenLijst": [x['teBereiken'] for x in details],
        "opgebouwdLijst": [x['opgebouwd'] for x in details],
        "afhandelingVereveningLijst": [x['afhandelingVerevening'] for x in details],
        "pensioenUitvoerderLijst": [x['pensioenUitvoerder'] for x in details],
        "herkenningsNummerLijst": [x['herkenningsNummer'] for x in details],
        "standPerLijst": [x['standPer'] for x in details],
    }

def make_PartnerPensioenPeriode(PartnerPensioen):
    (van, tot), details = PartnerPensioen
    return {
        "van": van,
        "tot": tot,
        "aantal": len(details),
        "verzekerdBedragLijst": [x['verzekerdBedrag'] for x in details],
        "opgebouwdBedragLijst": [x['opgebouwdBedrag'] for x in details],
        "verzekerdBedragNaPensLijst": [x['verzekerdBedragNaPens'] for x in details],
        "opgebouwdBedragNaPensLijst": [x['opgebouwdBedragNaPens'] for x in details],
        "afhandelingScheidingLijst": [x['afhandelingScheiding'] for x in details],
        "pensioenUitvoerderLijst": [x['pensioenUitvoerder'] for x in details],
        "herkenningsNummerLijst": [x['herkenningsNummer'] for x in details],
        "standPerLijst": [x['standPer'] for x in details],
    }

def make_WezenPensioenPeriode(WezenPensioen):
    (van, tot), details = WezenPensioen
    return {
        "van": van,
        "tot": tot,
        "aantal": len(details),
        "verzekerdBedragLijst": [x['verzekerdBedrag'] for x in details],
        "opgebouwdBedragLijst": [x['opgebouwdBedrag'] for x in details],
        "verzekerdBedragNaPensLijst": [x['verzekerdBedragNaPens'] for x in details],
        "opgebouwdBedragNaPensLijst": [x['opgebouwdBedragNaPens'] for x in details],
        "pensioenUitvoerderLijst": [x['pensioenUitvoerder'] for x in details],
        "perKindLijst": [x['perKind'] for x in details],
        "herkenningsNummerLijst": [x['herkenningsNummer'] for x in details],
        "standPerLijst": [x['standPer'] for x in details],
    }


def parse(mijnpensioenoverzicht):
    mijnpensioenoverzicht, AOWDetail, OuderdomsPensioenDetail, PartnerPensioenDetail, WezenPensioenDetail = common.parse_mijnpensioenoverzicht_details(mijnpensioenoverzicht)

    PensioenInfo = {
        "ouderdomsPensioenVanLijst" : [v for v,t in OuderdomsPensioenDetail.keys()],
        "ouderdomsPensioenTotLijst" : [t for v,t in OuderdomsPensioenDetail.keys()],
        "ouderdomsAantallenLijst" : [len(x) for x in OuderdomsPensioenDetail.values()],
        "partnerPensioenVanLijst" : [v for v,t in PartnerPensioenDetail.keys()],
        "partnerPensioenTotLijst" : [t for v,t in PartnerPensioenDetail.keys()],
        "partnerAantallenLijst" : [len(x) for x in PartnerPensioenDetail.values()],
        "wezenPensioenVanLijst" : [v for v,t in WezenPensioenDetail.keys()],
        "wezenPensioenTotLijst" : [t for v,t in WezenPensioenDetail.keys()],
        "wezenAantallenLijst" : [len(x) for x in WezenPensioenDetail.values()],
    }

    credentials = [{
      "credential": "irma-demo.inz-internetnz.membership",
      "attributes": {
        "id": json.dumps(PensioenInfo),
        "type": "PensioenInfo",
        "purchaseDate": "",
        "renewalDate": "",
        "validUntil": "",
      }
    }]

    OuderdomsPensioenDetail = list(OuderdomsPensioenDetail.items())
    PartnerPensioenDetail = list(PartnerPensioenDetail.items())
    WezenPensioenDetail = list(WezenPensioenDetail.items())
    
    if len(OuderdomsPensioenDetail) > 0:
        #P1OuderdomsPensioen = make_OuderdomsPensioenPeriode(OuderdomsPensioenDetail[0])
        credentials.append({
          "credential": "irma-demo.inz-internetnz.id",
          "attributes": {
            "number": json.dumps(make_OuderdomsPensioenPeriode(OuderdomsPensioenDetail[0])),
          }
        })
    #else:
        #P1OuderdomsPensioen = None

    if len(OuderdomsPensioenDetail) > 1:
        #P2OuderdomsPensioen = make_OuderdomsPensioenPeriode(OuderdomsPensioenDetail[1])
        credentials.append({
          "credential": "irma-demo.inz-personal-data.birthdate",
          "attributes": {
            "birthdate": json.dumps(make_OuderdomsPensioenPeriode(OuderdomsPensioenDetail[1])),
          }
        })
#    else:
#        P2OuderdomsPensioen = None

    if len(OuderdomsPensioenDetail) > 2:
        #P3OuderdomsPensioen = make_OuderdomsPensioenPeriode(OuderdomsPensioenDetail[2])
        credentials.append({
          "credential": "irma-demo.amsterdam.openStadBallot",
          "attributes": {
            "ballot": json.dumps(make_OuderdomsPensioenPeriode(OuderdomsPensioenDetail[2])),
          }
        })
#    else:
#        P3OuderdomsPensioen = None

    if len(OuderdomsPensioenDetail) > 3:
       # P4OuderdomsPensioen = make_OuderdomsPensioenPeriode(OuderdomsPensioenDetail[3])
        credentials.append({
          "credential": "irma-demo.MijnOverheid.root",
          "attributes": {
            "BSN": json.dumps(make_OuderdomsPensioenPeriode(OuderdomsPensioenDetail[3])),
          }
        })
#    else:
#        P4OuderdomsPensioen = None


    if len(PartnerPensioenDetail) > 0:
        #P1PartnerPensioen = make_PartnerPensioenPeriode(PartnerPensioenDetail[0])
        credentials.append({
          "credential": "irma-demo.inz-personal-data.personName",
          "attributes": {
            "fullName": json.dumps(make_PartnerPensioenPeriode(PartnerPensioenDetail[0])),
          }
        })
#    else:
#        P1PartnerPensioen = None

    if len(PartnerPensioenDetail) > 1:
        #P2PartnerPensioen = make_PartnerPensioenPeriode(PartnerPensioenDetail[1])
        credentials.append({
          "credential": "irma-demo.irmages.photos",
          "attributes": {
            "photo": json.dumps(make_PartnerPensioenPeriode(PartnerPensioenDetail[1])),
          }
        })
#    else:
#        P2PartnerPensioen = None

    if len(PartnerPensioenDetail) > 2:
        #P3PartnerPensioen = make_PartnerPensioenPeriode(PartnerPensioenDetail[2])
        credentials.append({
          "credential": "irma-demo.ivido.login",
          "attributes": {
            "identifier": json.dumps(make_PartnerPensioenPeriode(PartnerPensioenDetail[2])),
          }
        })
#    else:
#        P3PartnerPensioen = None

    if len(PartnerPensioenDetail) > 3:
        #P4PartnerPensioen = make_PartnerPensioenPeriode(PartnerPensioenDetail[3])
        credentials.append({
          "credential": "irma-demo.nijmegen.address",
          "attributes": {
            "street": json.dumps(make_PartnerPensioenPeriode(PartnerPensioenDetail[3])),
            "houseNumber": "",
            "zipcode": "",
            "municipality": "",
            "city": "",
          }
        })
#    else:
#        P4PartnerPensioen = None

    if len(WezenPensioenDetail) > 0:
        #P1WezenPensioen = make_WezenPensioenPeriode(WezenPensioenDetail[0])
        credentials.append({
          "credential": "irma-demo.inz-postal-service.address",
          "attributes": {
            "fullAddress": json.dumps(make_WezenPensioenPeriode(WezenPensioenDetail[0])),
          }
        })
#    else:
#        P1WezenPensioen = None

    if len(WezenPensioenDetail) > 1:
        P2WezenPensioen = make_WezenPensioenPeriode(WezenPensioenDetail[1])
        credentials.append({
          "credential": "irma-demo.suwinet.income",
          "attributes": {
            "income": json.dumps(make_WezenPensioenPeriode(WezenPensioenDetail[1])),
          }
        })
#    else:
#        P2WezenPensioen = None

    if len(WezenPensioenDetail) > 2:
        P3WezenPensioen = make_WezenPensioenPeriode(WezenPensioenDetail[2])
        credentials.append({
          "credential": "irma-demo.nijmegen.bsn",
          "attributes": {
            "bsn": json.dumps(make_WezenPensioenPeriode(WezenPensioenDetail[2])),
          }
        })
#    else:
#        P3WezenPensioen = None

    if len(WezenPensioenDetail) > 3:
        P4WezenPensioen = make_WezenPensioenPeriode(WezenPensioenDetail[3])
        credentials.append({
          "credential": "irma-demo.nijmegen.ageLimits",
          "attributes": {
            "over12": json.dumps(make_WezenPensioenPeriode(WezenPensioenDetail[3])),
            "over16": "",
            "over18": "",
            "over21": "",
            "over65": "",
          }
        })
#    else:
#        P4WezenPensioen = None
    
#    return {
#       "PensioenInfo": PensioenInfo,
#       "P1OuderdomsPensioen": P1OuderdomsPensioen,
#       "P1PartnerPensioen": P1PartnerPensioen,
#       "P1WezenPensioen": P1WezenPensioen,
#       "P2OuderdomsPensioen": P2OuderdomsPensioen,
#       "P2PartnerPensioen": P2PartnerPensioen,
#       "P2WezenPensioen": P2WezenPensioen,
#       "P3OuderdomsPensioen": P3OuderdomsPensioen,
#       "P3PartnerPensioen": P3PartnerPensioen,
#       "P3WezenPensioen": P3WezenPensioen,
#       "P4OuderdomsPensioen": P4OuderdomsPensioen,
#       "P4PartnerPensioen": P4PartnerPensioen,
#       "P4WezenPensioen": P4WezenPensioen,
#    }
    return {
      "@context": "https://irma.app/ld/request/issuance/v2",
      "credentials": credentials
    }

def print(mijnpensioenoverzicht):
    import pprint
    pp = pprint.PrettyPrinter(indent=4, sort_dicts=False)

    print("PensioenInfo")
    pp.pprint(PensioenInfo)
    print("P1OuderdomsPensioen")
    pp.pprint(P1OuderdomsPensioen)
    print("P2OuderdomsPensioen")
    pp.pprint(P2OuderdomsPensioen)
    print("P3OuderdomsPensioen")
    pp.pprint(P3OuderdomsPensioen)
    print("P4OuderdomsPensioen")
    pp.pprint(P4OuderdomsPensioen)

    print("P1PartnerPensioen")
    pp.pprint(P1PartnerPensioen)
    print("P2PartnerPensioen")
    pp.pprint(P2PartnerPensioen)
    print("P3PartnerPensioen")
    pp.pprint(P3PartnerPensioen)
    print("P4PartnerPensioen")
    pp.pprint(P4PartnerPensioen)

    print("P1WezenPensioen")
    pp.pprint(P1WezenPensioen)
    print("P2WezenPensioen")
    pp.pprint(P2WezenPensioen)
    print("P3WezenPensioen")
    pp.pprint(P3WezenPensioen)
    print("P4WezenPensioen")
    pp.pprint(P4WezenPensioen)


import json
import requests

def parse_event(x):
    if 'Leeftijd' in x:
        leeftijd = x['Leeftijd']
        return f"Leeftijd(Jaren={leeftijd['Jaren']},Maanden={leeftijd['Maanden']})"
    else:
        return list(x.values())[0]

def load_mijnpensioenoverzicht(mijnpensioenoverzicht_path):
    with open(mijnpensioenoverzicht_path, 'r') as f:
        mijnpensioenoverzicht = json.load(f)
    return mijnpensioenoverzicht

# TODO can this used instead of load_mijnpensioenoverzicht?
def load_file(path):
    with open(path, 'r') as f:
        return json.load(f)

def read_mijnpensioenoverzicht(mijnpensioenoverzicht_str):
    return json.loads(mijnpensioenoverzicht_str)

def parse_mijnpensioenoverzicht_details(mijnpensioenoverzicht):
    AOWDetail, OuderdomsPensioenDetail = parse_OAWDetail_OuderdomsPensioenDetail(mijnpensioenoverzicht)
    PartnerPensioenDetail = parse_PartnerPensioenDetail(mijnpensioenoverzicht)
    WezenPensioenDetail= parse_WezenPensioenDetail(mijnpensioenoverzicht)
    return mijnpensioenoverzicht, AOWDetail, OuderdomsPensioenDetail, PartnerPensioenDetail, WezenPensioenDetail

def parse_mijnpensioenoverzicht_totalen(mijnpensioenoverzicht,
        OuderdomsPensioenDetail, PartnerPensioenDetail, WezenPensioenDetail):
    OuderdomsPensioenTotaal = parse_OuderdomsPensioenTotaal(mijnpensioenoverzicht, OuderdomsPensioenDetail)
    PartnerPensioenTotaal = parse_PartnerPensioenTotaal(mijnpensioenoverzicht, PartnerPensioenDetail)
    WezenPensioenTotaal = parse_WezenPensioenTotaal(mijnpensioenoverzicht, WezenPensioenDetail)
    return OuderdomsPensioenTotaal, PartnerPensioenTotaal, WezenPensioenTotaal

def parse_OAWDetail_OuderdomsPensioenDetail(mijnpensioenoverzicht):
    AOWDetail = {}
    # de rest allemaal in de vorm: (van,tot) => attributes
    OuderdomsPensioenDetail = {} 
    # OuderdomsPensioenDetail en AOWDetail
    for item in mijnpensioenoverzicht['Details']['OuderdomsPensioenDetails']['OuderdomsPensioen']:
        van = parse_event(item['Van'])
        tot = parse_event(item['Tot'])
        if 'AOW' in item:
            assert AOWDetail == {}
            aow = item['AOW']['AOWDetailsOpbouw']
            AOWDetail = {
                "van": van,
                "tot": tot,
                "standPer": item['AOW']['StandPer'],
                "teBereikenSamenwonend": aow['TeBereikenSamenwonend'],
                "teBereikenAlleenstaand": aow['TeBereikenAlleenstaand'],
                "opgebouwdSamenwonend": aow['OpgebouwdSamenwonend'],
                "opgebouwdAlleenstaand": aow['OpgebouwdAlleenstaand'],
            }
        for pensioen in item['Pensioen']:
            lijst = OuderdomsPensioenDetail.get((van,tot),[])
            lijst.append({
                "van": van,
                "tot": tot,
                "teBereiken": pensioen['TeBereiken'],
                "opgebouwd": pensioen['Opgebouwd'],
                "afhandelingVerevening": pensioen.get('AfhandelingVerevening', ""),
                "pensioenUitvoerder": pensioen['PensioenUitvoerder'],
                "herkenningsNummer": pensioen['HerkenningsNummer'],
                "standPer": pensioen['StandPer'],
            })
            OuderdomsPensioenDetail[(van,tot)] = lijst
    return AOWDetail, OuderdomsPensioenDetail

def parse_OuderdomsPensioenTotaal(mijnpensioenoverzicht, OuderdomsPensioenDetail):
    OuderdomsPensioenTotaal = {}
    # OuderdomsPensioenTotaal
    for item in mijnpensioenoverzicht['Totalen']['OuderdomsPensioenTotalen']['OuderdomsPensioenTotaal']:
        van = parse_event(item['Van'])
        tot = parse_event(item['Tot'])
        OuderdomsPensioenTotaal[(van,tot)] = {
            "van": van,
            "tot": tot,
            "pensioen": item['Pensioen'],
            "aantal": len(OuderdomsPensioenDetail[(van,tot)]),
        }
    return OuderdomsPensioenTotaal

def parse_PartnerPensioenDetail(mijnpensioenoverzicht):
    PartnerPensioenDetail = {}
    # PartnerPensioenDetail
    for item in mijnpensioenoverzicht['Details']['PartnerPensioenDetails']['PartnerPensioen']:
        van = parse_event(item['Van'])
        tot = parse_event(item['Tot'])
        for pensioen in item['Pensioen']:
            bedragen = pensioen['Bedragen']
            lijst = PartnerPensioenDetail.get((van,tot),[])
            lijst.append({
                "van": van,
                "tot": tot,
                "verzekerdBedrag": bedragen['VerzekerdBedrag'],
                "opgebouwdBedrag": bedragen['OpgebouwdBedrag'],
                "verzekerdBedragNaPens": bedragen['VerzekerdBedragNaPens'],
                "opgebouwdBedragNaPens": bedragen['OpgebouwdBedragNaPens'],
                "afhandelingScheiding": pensioen.get('AfhandelingScheiding', ""),
                "pensioenUitvoerder": pensioen['PensioenUitvoerder'],
                "herkenningsNummer": pensioen['HerkenningsNummer'],
                "standPer": pensioen['StandPer'],
            })
            PartnerPensioenDetail[(van,tot)] = lijst
    return PartnerPensioenDetail

def parse_PartnerPensioenTotaal(mijnpensioenoverzicht, PartnerPensioenDetail):
    PartnerPensioenTotaal = {}
    # PartnerPensioenTotaal
    for item in mijnpensioenoverzicht['Totalen']['PartnerPensioenTotalen']['PartnerPensioenTotaal']:
        van = parse_event(item['Van'])
        tot = parse_event(item['Tot'])
        pensioen = item['Pensioen']
        PartnerPensioenTotaal[(van,tot)] = {
            "van": van,
            "tot": tot,
            "verzekerdBedrag": pensioen['VerzekerdBedrag'],
            "opgebouwdBedrag": pensioen['OpgebouwdBedrag'],
            "verzekerdBedragNaPens": pensioen['VerzekerdBedragNaPens'],
            "opgebouwdBedragNaPens": pensioen['OpgebouwdBedragNaPens'],
            "aantal": len(PartnerPensioenDetail[(van,tot)]),
        }
    return PartnerPensioenTotaal

def parse_WezenPensioenDetail(mijnpensioenoverzicht):
    WezenPensioenDetail = {}
    # WezenPensioenDetail
    for item in mijnpensioenoverzicht['Details']['WezenPensioenDetails']['WezenPensioen']:
        van = parse_event(item['Van'])
        tot = parse_event(item['Tot'])
        for pensioen in item['Pensioen']:
            bedragen = pensioen['Bedragen']
            lijst = WezenPensioenDetail.get((van,tot),[])
            lijst.append({
                "van": van,
                "tot": tot,
                "verzekerdBedrag": bedragen['VerzekerdBedrag'],
                "opgebouwdBedrag": bedragen['OpgebouwdBedrag'],
                "verzekerdBedragNaPens": bedragen['VerzekerdBedragNaPens'],
                "opgebouwdBedragNaPens": bedragen['OpgebouwdBedragNaPens'],
                "pensioenUitvoerder": pensioen['PensioenUitvoerder'],
                "perKind": pensioen['PerKind'],
                "herkenningsNummer": pensioen['HerkenningsNummer'],
                "standPer": pensioen['StandPer'],
            })
            WezenPensioenDetail[(van,tot)] = lijst
        return WezenPensioenDetail

def parse_WezenPensioenTotaal(mijnpensioenoverzicht, WezenPensioenDetail):
    WezenPensioenTotaal = {}
    # WezenPensioenTotaal
    for item in mijnpensioenoverzicht['Totalen']['WezenPensioenTotalen']['WezenPensioenTotaal']:
        van = parse_event(item['Van'])
        tot = parse_event(item['Tot'])
        bedragenPerKind = item['Pensioen']['BedragenPerKind']
        WezenPensioenTotaal[(van,tot)] = {
            "van": van,
            "tot": tot,
            "verzekerdBedragPerKind": bedragenPerKind['VerzekerdBedrag'],
            "opgebouwdBedragPerKind": bedragenPerKind['OpgebouwdBedrag'],
            "verzekerdBedragNaPensPerKind": bedragenPerKind['VerzekerdBedragNaPens'],
            "opgebouwdBedragNaPensPerKind": bedragenPerKind['OpgebouwdBedragNaPens'],
            # TODO: Bij deze telling wordt nog geen rekening gehouden of echt om 'PerKind' bedragen gaat.
            "aantalPerKind": len(WezenPensioenDetail[(van,tot)]),
        }
    return WezenPensioenTotaal

# Singletons:
# 5 attrs:  irma-demo.digidproef.address
# 11 attrs: irma-demo.digidproef.basicPersonalData
# 17 attrs: irma-demo.digidproef.personalData

# 4 attrs:  irma-demo.MijnOverheid.ageHigher
# 4 attrs:  irma-demo.MijnOverheid.ageLower
# 4 attrs:  irma-demo.MijnOverheid.birthCertificateI
# 4 attrs:  irma-demo.MijnOverheid.fullName
# 1 attrs:  irma-demo.MijnOverheid.root

# 4 attrs:  irma-demo.RU.studentCard

# 1 attrs:  irma-demo.amsterdam.openStadBallot

# 3 attrs:  irma-demo.amsterdamVorin.taxiZoneExemption  (Revocation)

# 6 attrs:  irma-demo.chipsoft.bsn

# 9 attrs:  irma-demo.cis.complianceCheck
# 8 attrs:  irma-demo.cis.digitalInsurancePassport

# 5 attrs:  irma-demo.gemeente.address
# 18 attrs: irma-demo.gemeente.personalData

# 8 attrs:  irma-demo.ggd.coronatest

# 5 attrs:  irma-demo.idin.ageLimits
# 13 attrs: irma-demo.idin.idin

# 10 attrs: irma-demo.inz-id-card.idCard (Revocation)
# 1 attrs:  irma-demo.inz-internetnz.id
# 5 attrs:  irma-demo.inz-internetnz.membership
# 2 attrs:  irma-demo.inz-personal-data.ageLimits
# 1 attrs:  irma-demo.inz-personal-data.birthdate
# 1 attrs:  irma-demo.inz-personal-data.personName
# 1 attrs:  irma-demo.inz-postal-service.address
# 1 attrs:  irma-demo.inz-revocable-id.id (Revocation)

# 1 attrs:      irma-demo.irmages.photos

# 4 attrs:      irma-demo.irvn.account

# 1 attrs:      irma-demo.ivido.login

# 1 attrs:      irma-demo.suwinet.income

# 5 attrs:      irma-demo.nijmegen.address
# 5 attrs:      irma-demo.nijmegen.ageLimits
# 1 attrs:      irma-demo.nijmegen.bsn
# 9 attrs:      irma-demo.nijmegen.personalData

# 7 attrs:      irma-demo.vzvz.healthcareidentity

## Multiple

# 44 attrs: irma-demo.signicat.kvkTradeRegister

def get_issue_session(yivi_issue_dict, yivi_server="http://localhost:8088", ):
#    import pprint
#    pp = pprint.PrettyPrinter(indent=4, sort_dicts=False)
#    pp.pprint(yivi_issue_dict)
    resp = requests.post(f'{yivi_server}/session', json=yivi_issue_dict)
    return resp.content.decode()

import itertools

from . import common

KEYS_CREDENTIAL = "irma-demo.discipl-pra.demoKeys"
CREDENTIAL = "irma-demo.discipl-pra.demoValue"
KEY = "key"
VALUE = "value"
VERSION = "version"

def parse2attributes(v, prefix=""):
    if type(v) == list:
        results = []
        for i, v2 in enumerate(v):
            results.extend(parse2attributes(v2, prefix=f"{prefix}<{i}"))
        return results
    elif type(v) == dict:
        results = []
        for k, v2 in v.items():
            results.extend(parse2attributes(v2, prefix=f"{prefix}>{k}"))
        return results
    else:
        return [{KEY: prefix, VALUE: str(v)}]

def attributes2requestbody(attributes, current_keys):
    current_keys = current_keys.copy()
    
    for attr in attributes:
        attrkey = attr[KEY]
        new_version = current_keys.get(attrkey, 0)+1
        current_keys[attrkey] = new_version
        attr[VERSION] = str(new_version)

    credentials = [{
      "credential": KEYS_CREDENTIAL,
      "attributes": {
        "keys": ";\n".join(current_keys.keys()),
        "versions": ";".join(str(x) for x in current_keys.values()),
      },
    }] + [{
      "credential": CREDENTIAL,
      "attributes": a,
    } for a in attributes]

    print("n credentials: ", len(credentials))

    return {
      "@context": "https://irma.app/ld/request/issuance/v2",
      "credentials": credentials,
    }, current_keys



def parse(mijnpensioenoverzicht, current_keys={}):
    _, AOWDetail, OuderdomsPensioenDetail, PartnerPensioenDetail, WezenPensioenDetail = common.parse_mijnpensioenoverzicht_details(mijnpensioenoverzicht)

    OuderdomsPensioenTotaal, PartnerPensioenTotaal, WezenPensioenTotaal = common.parse_mijnpensioenoverzicht_totalen(mijnpensioenoverzicht, OuderdomsPensioenDetail, PartnerPensioenDetail, WezenPensioenDetail)

    OuderdomsPensioenDetail = list(itertools.chain(*OuderdomsPensioenDetail.values())) 
    OuderdomsPensioenTotaal = list(OuderdomsPensioenTotaal.values())
    PartnerPensioenDetail = list(itertools.chain(*PartnerPensioenDetail.values()))
    PartnerPensioenTotaal = list(PartnerPensioenTotaal.values())
    WezenPensioenDetail = list(itertools.chain(*WezenPensioenDetail.values()))
    WezenPensioenTotaal = list(WezenPensioenTotaal.values())

    attributes = []
    attributes.extend(parse2attributes(AOWDetail, prefix="mijnpensioenoverzicht>aowDetail"))
    attributes.extend(parse2attributes(OuderdomsPensioenDetail, prefix="mijnpensioenoverzicht>ouderdomsPensioenDetail"))
    attributes.extend(parse2attributes(PartnerPensioenDetail, prefix="mijnpensioenoverzicht>partnerPensioenDetail"))
#    attributes.extend(parse2attributes(WezenPensioenDetail, prefix="mijnpensioenoverzicht>wezenPensioenDetail"))
#    attributes.extend(parse2attributes(OuderdomsPensioenTotaal, prefix="mijnpensioenoverzicht>ouderdomsPensioenTotaal"))
#    attributes.extend(parse2attributes(PartnerPensioenTotaal, prefix="mijnpensioenoverzicht>partnerPensioenTotaal"))
#    attributes.extend(parse2attributes(WezenPensioenTotaal, prefix="mijnpensioenoverzicht>wezenPensioenTotaal"))
    return attributes2requestbody(attributes, current_keys)
   

def parse_dict(d, data_name, current_keys={}):
    attributes = parse2attributes(d, prefix=data_name)
    return attributes2requestbody(attributes, current_keys)


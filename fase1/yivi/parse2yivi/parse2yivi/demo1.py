import itertools

from . import common

def parse(mijnpensioenoverzicht):
    mijnpensioenoverzicht, AOWDetail, OuderdomsPensioenDetail, PartnerPensioenDetail, WezenPensioenDetail = common.parse_mijnpensioenoverzicht_details(mijnpensioenoverzicht)

    OuderdomsPensioenTotaal, PartnerPensioenTotaal, WezenPensioenTotaal = common.parse_mijnpensioenoverzicht_totalen(mijnpensioenoverzicht, OuderdomsPensioenDetail, PartnerPensioenDetail, WezenPensioenDetail)

    OuderdomsPensioenDetail = list(itertools.chain(*OuderdomsPensioenDetail.values())) 
    OuderdomsPensioenTotaal = list(itertools.chain(*OuderdomsPensioenTotaal.values()))
    PartnerPensioenDetail = list(itertools.chain(*PartnerPensioenDetail.values()))
    PartnerPensioenTotaal = list(itertools.chain(*PartnerPensioenTotaal.values()))
    WezenPensioenDetail = list(itertools.chain(*WezenPensioenDetail.values()))
    WezenPensioenTotaal = list(itertools.chain(*WezenPensioenTotaal.values()))
    return {
        "OuderdomsPensioenDetail": OuderdomsPensioenDetail,
        "OuderdomsPensioenTotaal": OuderdomsPensioenTotaal,
        "PartnerPensioenDetail": PartnerPensioenDetail,
        "PartnerPensioenTotaal": PartnerPensioenTotaal,
        "WezenPensioenDetail": WezenPensioenDetail,
        "WezenPensioenTotaal": WezenPensioenTotaal,
    }


def print(mijnpensioenoverzicht):
    mijnpensioenoverzicht, AOWDetail, OuderdomsPensioenDetail, PartnerPensioenDetail, WezenPensioenDetail = common.parse_mijnpensioenoverzicht_details(mijnpensioenoverzicht)

    OuderdomsPensioenTotaal, PartnerPensioenTotaal, WezenPensioenTotaal = common.parse_mijnpensioenoverzicht_totalen(mijnpensioenoverzicht, OuderdomsPensioenDetail, PartnerPensioenDetail, WezenPensioenDetail)

    OuderdomsPensioenDetail = list(itertools.chain(*OuderdomsPensioenDetail.values())) 
    OuderdomsPensioenTotaal = list(itertools.chain(*OuderdomsPensioenTotaal.values()))
    PartnerPensioenDetail = list(itertools.chain(*PartnerPensioenDetail.values()))
    PartnerPensioenTotaal = list(itertools.chain(*PartnerPensioenTotaal.values()))
    WezenPensioenDetail = list(itertools.chain(*WezenPensioenDetail.values()))
    WezenPensioenTotaal = list(itertools.chain(*WezenPensioenTotaal.values()))

    import pprint
    pp = pprint.PrettyPrinter(indent=4, sort_dicts=False)

    print("AOWDetail")
    pp.pprint(AOWDetail)
    print("OuderdomsPensioenDetail")
    pp.pprint(OuderdomsPensioenDetail)
    print("OuderdomsPensioenTotaal")
    pp.pprint(OuderdomsPensioenTotaal)
    print("PartnerPensioenDetail")
    pp.pprint(PartnerPensioenDetail)
    print("PartnerPensioenTotaal")
    pp.pprint(PartnerPensioenTotaal)
    print("WezenPensioenDetail")
    pp.pprint(WezenPensioenDetail)
    print("WezenPensioenTotaal")
    pp.pprint(WezenPensioenTotaal)



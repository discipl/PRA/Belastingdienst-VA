using Stipple
using StippleUI
using HTTP

using PythonCall
common = pyimport("parse2yivi.common")
parser = pyimport("parse2yivi.demo3")

@vars Model begin
  channel::String = "", READONLY
  valid_upload::Bool = false, READONLY
  pdf::Bool = false
  json::Bool = false
  yivi::Bool = false
  #filename::String = "", READONLY
  title::String = ""
end

Stipple.js_methods(::Model) = """
  issue_yivi_credentials: function() {
    const options = {
      // Developer options
      debugging: true,
      // Front-end options
      language:  'en',
      translations: {
        header:  `\${this.title}<i class="irma-web-logo">IRMA</i>`,
        loading: 'Bezig...'
      },
      session: {
        // Point this to your IRMA server:
        url: 'http://localhost:8000/irmaserver',
        // Define your issuing request:
        start: {
          url:           o => `http://localhost:8000/irmaserver/session?channel=\${this.channel}`,
          parseResponse: r => r.json()
        }
      }
    };
//    const irmaWeb = irma.newWeb({
//      ...options,
//      element: '#irma-web-form'
//    });
//    irmaWeb.start()
//    .then(result => console.log("Successful issued! 🎉", result))
//    .catch(error => {
//        if (error === 'Aborted') {
//          console.log('We closed it ourselves, so no problem 😅');
//          return;
//        }
//        console.error("Couldn't do what you asked 😢", error);
//      });
//    document.getElementById('abort-web').onclick = () => irmaWeb.abort();
    let irmaPopup = irma.newPopup(options);
//    document.getElementById('start-popup').onclick = () => {
      irmaPopup.start()
      .then(result => console.log("Successful disclosure! 🎉", result))
      .catch(error => {
        if (error === 'Aborted') {
          console.log('We closed it ourselves, so no problem 😅');
          return;
        }
        console.error("Couldn't do what you asked 😢", error);
      })
      .finally(() => irmaPopup = irma.newPopup(options));
//    };
  }
"""

Stipple.js_watch(::Model) = """
  valid_upload: function(newval, oldval) {
    if (newval) this.issue_yivi_credentials()
  }
"""

function handlers(model)
  on(model.isready) do isready
    model.channel[] = model.channel__
    model.title[] = "Mijn Pensioenoverzicht"
  end
#  onbutton(model.process) do
#      model.valid_upload[] = false
#      model.valid_upload[] = true
#  end
  onbutton(model.yivi) do
    #channel = model.channel[]
    channel = "demo"
    if channel in keys(current_key_versions_dict)
        current_key_versions = current_key_versions_dict[channel]
        issue_request_payload, current_key_versions = parser.parse(common.load_mijnpensioenoverzicht("../../mijnpensioenoverzicht.json"), current_keys=current_key_versions)
    else
        issue_request_payload, current_key_versions = parser.parse(common.load_mijnpensioenoverzicht("../../mijnpensioenoverzicht.json"))
    end
    parsed_yivi_credentials[channel] = issue_request_payload
    current_key_versions_dict[channel] = current_key_versions
    model.valid_upload[] = true
    model.valid_upload[] = false
  end
  model
end

function handlers_VA(model)
  on(model.isready) do isready
    model.channel[] = model.channel__
    model.title[] = "Belastingdienst: Voorlopige Aanslag"
  end
#  onbutton(model.process) do
#      model.valid_upload[] = false
#      model.valid_upload[] = true
#  end
  onbutton(model.yivi) do
    #channel = model.channel[]
    channel = "demo"
    if channel in keys(current_key_versions_dict)
        current_key_versions = current_key_versions_dict[channel]
        issue_request_payload, current_key_versions = parser.parse_dict(common.load_file("../../voorlopige_aanslag.json"), "VA", current_keys=current_key_versions)
    else
        issue_request_payload, current_key_versions = parser.parse_dict(common.load_file("../../voorlopige_aanslag.json"), "VA")
    end
    parsed_yivi_credentials[channel] = issue_request_payload
    current_key_versions_dict[channel] = current_key_versions
    model.valid_upload[] = true
    model.valid_upload[] = false
  end
  model
end


function ui(channel)
  row(cell(class = "st-module", [
#    card(class = "q-my-md", [
#        card_section("channel: {{ channel }}")
#        card_section("valid_upload: {{ valid_upload }}")
#    ])
    uploader(multiple = false, accept = ".json", method = "POST", field__name = "file", url = "/jsonupload?channel=$channel",
      hide__upload__btn = true, auto__upload = true, 
      #form__fields! = "[{name: 'channel', value: channel}]",
      #hide__upload__btn! = "channel == ''", auto__upload! = "channel != ''", disable! = "!isready"
    )
#    btn(class = "q-my-md", "Reload", @click(:process), color = "primary")
#    btn(class = "q-my-md", "Get QR", @on("click", "issue_yivi_credentials()"), color = "primary") 
#    """<section class="irma-web-form" id="irma-web-form"></section>"""
#     """
#      <button id="abort-web">Abort web element</button>
#      <div>Or</div>
#      <button id="start-popup">Start as popup</button>
#    """
  ]))
end

function choices_ui()
  row(cell(class = "st-module", [
    card(class = "q-my-md", [
      h4("Kies het formaat waarin u uw <i>Mijn Pensioenoverzicht</i> wilt verkrijgen."),
      card_section(row([
        cell(btn(class = "q-my-md", "PDF Bestand", @click(:pdf), color = "primary", :no__caps, icon="download"))
        cell(btn(class = "q-my-md", "JSON Bestand", @click(:json), color = "primary", :no__caps, icon="download"))
        cell(btn(class = "q-my-md", "Yivi Attributen", @click(:yivi), color = "primary", :no__caps, icon="qr_code"))
      ]))
    ])
  ]))
end

function choices_ui_VA()
  row(cell(class = "st-module", [
    card(class = "q-my-md", [
      h4("Kies het formaat waarin u uw <i>Voorlopige Aanslag</i> wilt verkrijgen."),
      card_section([
        row([
          cell(btn(class = "q-my-md", "PDF Bestand", @click(:pdf), color = "primary", :no__caps, icon="download"))
        ])
        row([
          cell(btn(class = "q-my-md", "JSON Bestand", @click(:json), color = "primary", :no__caps, icon="download"))
        ])
        row([
          cell(btn(class = "q-my-md", "Yivi Attributen", @click(:yivi), color = "primary", :no__caps, icon="qr_code"))
        ])
      ])
    ])
  ]))
end

route("/mijn-pensioenoverzicht-downloaden") do
  model = init(Model)
  handlers(model)
  page(model, choices_ui()) * """<script type="text/javascript" src="/irma.js"></script>""" |> html
end

route("/voorlopige-aanslag-downloaden") do
  model = init(Model)
  handlers_VA(model)
  page(model, choices_ui_VA()) * """<script type="text/javascript" src="/irma.js"></script>""" |> html
end

const models = Dict{String, Model}()
const parsed_yivi_credentials = Dict()
const current_key_versions_dict = Dict()

route("/upload") do
  channel = params(:channel, "")
  channel == "" && return Genie.Renderer.redirect("?channel=$(Stipple.channelfactory())")
  if channel ∈ keys(models)
    model = models[channel]
  else
    model = init(Model, channel=channel)
    handlers(model)
    models[channel] = model
  end
  page(model, ui(channel)) * """<script type="text/javascript" src="/irma.js"></script>""" |> html
end


#uploading csv files to the backend server
route("/jsonupload", method="POST") do
  files = Genie.Requests.filespayload()
  #channel = post(:channel)
  channel = params(:channel)
  model = models[channel]
#  @show Genie.Requests.postpayload()
#  @show Genie.Requests.getpayload()
  file_str = String(first(values(files)).data)
  issue_request_payload = parser.parse(common.read_mijnpensioenoverzicht(file_str))
  parsed_yivi_credentials[channel] = issue_request_payload
  model.valid_upload[] = true
  model.valid_upload[] = false
  return html()
end

route("/irmaserver/session") do
  #channel = params(:channel)
  channel = "demo"
  issue_request_payload = parsed_yivi_credentials[channel]
  return common.get_issue_session(issue_request_payload)
end

route("/irmaserver/session/:requestor_token/result") do
    HTTP.get("http://localhost:8088/session/$(Genie.Requests.payload(:requestor_token))/result")
end

route("/") do
    Genie.Renderer.redirect("/links.html")
end


up(async=false)

#Genie.isrunning(:webserver) || up()

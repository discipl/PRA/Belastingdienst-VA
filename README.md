# Belastingdienst Pensioencase - PRA 

## Description

This folder contains files created and used for prototyping the PRA and only the files which are specifically created in the context of the use case of the Dutch Tax Administration.
You can find the following in this repository:

 - fase1 : python implementation for a stripped down income tax calculation, example personal data export from mijnpensioenoverzicht.nl, files for demonstrating the Yivi wallet in combination with the PRA, all of which were part of a first phase in the project (until summer 2023)
 - experimenten : experiments on compliance by design in second phase of the project (until summer 2024)
